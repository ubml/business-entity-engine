package com.inspur.edp.bef.engine.repository;

import org.junit.jupiter.api.Test;
import org.springframework.format.datetime.DateFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateFormatterTest {
    @Test
    public void parseTest() throws ParseException {
        DateFormatter dateFormatterFor8Pattern = new DateFormatter();
        dateFormatterFor8Pattern.setPattern("yyyyMMdd");

        Date date = dateFormatterFor8Pattern.parse("20240808", Locale.getDefault());
        System.out.println(date);
        // 记录开始时间
        long startTime = System.currentTimeMillis();

        // 执行你的代码
        // 例如：
        for (int i = 0; i < 10000; i++) {
            Date date1 = dateFormatterFor8Pattern.parse("20240808", Locale.getDefault());
        }
        // 记录结束时间
        long endTime = System.currentTimeMillis();
        // 计算并输出耗时
        long duration = endTime - startTime;
        System.out.println("耗时: " + duration + " 毫秒。");
    }

    @Test
    public void parse2Test() throws ParseException {
        SimpleDateFormat sdfForYYYYMMDD = new SimpleDateFormat("yyyyMMdd");


        Date date = sdfForYYYYMMDD.parse("20240808");
        System.out.println(date);
        // 记录开始时间
        long startTime = System.currentTimeMillis();

        // 执行你的代码
        // 例如：
        for (int i = 0; i < 10000; i++) {
            Date date1 =  sdfForYYYYMMDD.parse("20240808");
        }
        // 记录结束时间
        long endTime = System.currentTimeMillis();
        // 计算并输出耗时
        long duration = endTime - startTime;
        System.out.println("耗时: " + duration + " 毫秒。");
    }
}
