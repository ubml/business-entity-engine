package com.inspur.edp.bef.engine.repository.util;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.entity.GspCommonModel;

/**
 * @className: 异常提示工具类
 * @author: wangmj
 * @date: 2024/6/20
 **/
public class ExceptionUtil {
    /**
     * 获取异常提示中的BE描述信息
     * @param gspCommonModel
     * @return
     */
    public static String getBEInfo(GspCommonModel gspCommonModel){
        if(gspCommonModel == null){
            return "null";
        }
        String beInfo = String.format("Id:%s 编号:%s",gspCommonModel.getID(), gspCommonModel.getCode());
        return beInfo;
    }

    /**
     * 获取异常提示中的BE描述信息
     * @param bizEntityObject
     * @return
     */
    public static String getBEInfo(IGspCommonObject bizEntityObject){
        if(bizEntityObject == null || bizEntityObject.getBelongModel() == null){
            return "null";
        }
        String beInfo = String.format("Id:%s 编号:%s",bizEntityObject.getBelongModel().getID(), bizEntityObject.getBelongModel().getCode());
        return beInfo;
    }

    /**
     * 获取异常提示中的对象描述信息
     * @param commonObject
     * @return
     */
    public static String getBizEntityObjInfo(IGspCommonObject commonObject){
        if(commonObject == null){
            return "null";
        }
        String bizObjInfo = String.format("ID:%s 编号:%s",commonObject.getID(), commonObject.getCode());
        return bizObjInfo;
    }

    /**
     * 获取异常提示中的字段描述信息
     * @param element
     * @return
     */
    public static String getBizElementInfo(IGspCommonField element){
        if(element == null){
            return "null";
        }
        String bizObjInfo = "";
        if(element instanceof IGspCommonElement){
            bizObjInfo = String.format("ID:%s, 编号:%s, ColumnID:%s", element.getID() , element.getLabelID() ,((IGspCommonElement)element).getColumnID());
        }
        else{
            bizObjInfo = String.format("ID:%s, 编号:%s", element.getID() , element.getLabelID());
        }
        return bizObjInfo;
    }

    /**
     * 获取异常提示中的关联描述信息
     * @param association
     * @return
     */
    public static String getAssociationInfo(GspAssociation association){
        String assoInfo = String.format("关联ID:%s 关联模型ID：%s 关联对象ID:%s",
                association.getId(),
                association.getRefModelID(),
                association.getRefObjectID());
        return assoInfo;
    }
}
