/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.repository.repos;

import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.engine.repository.dac.MainObjectDac;
import com.inspur.edp.cef.repository.dac.MainEntityDac;
import com.inspur.edp.cef.repository.repo.BaseRootRepository;
import com.inspur.edp.cef.repository.repo.RepositoryExtendAttribute;

@RepositoryExtendAttribute(com.inspur.edp.customize.repository.CustomizeExtendInfo.class)
public class BefRepository extends BaseRootRepository {
    private GspBusinessEntity be;

    public BefRepository(GspBusinessEntity be) {
        this.be = be;
    }

    public BefRepository(String metadataId) {
    }

    @Override
    protected MainEntityDac getMainEntityDac() {
        //这个地方如果缓存，考虑adaptor的缓存使用。。
        MainEntityDac mainEntityDac = new MainObjectDac(be);
        mainEntityDac.initRepoVariables(this.vars);
        return mainEntityDac;
    }
}
