package com.inspur.edp.bef.engine.repository.exception;

public class ErrorCodes {
    /**
     * 枚举字段[{0}]进行数据转换时出错,传入值类型不正确，只支持Integer和String类型，当前传入值为[{1}],类型为[{2}]请检查。所属BE[{3}]，对象[{4}],字段[{5}]
     */
    public static String BEF_ENGINE_2001 = "BEF_ENGINE_2001";
    /**
     * {0}对应枚举索引类型不支持：{1}
     */
    public static String BEF_ENGINE_2002 = "BEF_ENGINE_2002";
    /**
     * 找不到字段[{0}]对应的数据库列，请检查BE元数据和dbo是否正确部署，对应BE:[{1}],对象:[{2}]，字段:[{3}], dbo:[{4}]
     */
    public static String BEF_ENGINE_2003 = "BEF_ENGINE_2003";
    /**
     * 字段{0}没有合适的读取方法，类型为{1},对应BE:[{2}],对象:[{3}]，字段:[{4}]
     */
    public static String BEF_ENGINE_2004 = "BEF_ENGINE_2004";
    /**
     * 字段[{0}]为枚举类型，但未获取到枚举项，请检查元数据设置。对应BE:[{1}],对象:[{2}],字段:[{3}]
     */
    public static String BEF_ENGINE_2005 = "BEF_ENGINE_2005";
    /**
     * 未在枚举字段[{0}]中获取到枚举项，当前支持传入枚举项索引或者编号，请检查输入参数是否正确。当前传入值为[{1}],对应BE:[{2}],对象:[{3}]，字段:[{4}]
     */
    public static String BEF_ENGINE_2006 = "BEF_ENGINE_2006";
    /**
     * dbo{0}中找不到字段{1}对应的列{2}columnid
     */
    public static String BEF_ENGINE_2007 = "BEF_ENGINE_2007";
    /**
     * 模型{0}上的对象{1}对应的dbo没有获取到，请检查dbo是否成功部署到正确的数据库中，dboId为{2}
     */
    public static String BEF_ENGINE_2008 = "BEF_ENGINE_2008";
    /**
     * 通过io.iec.edp.caf.databaseobject.api.entity.DatabaseObjectTableCore.getColumnById()方法
     * 在code为{0}的dbo中找不到columnid为{1}的dbo列,BE字段编号为{2}BE元数据ID编号分别为{3}{4}
     * 请检查BE元数据和DBO是否均已正确部署,BE元数据部署后保存在gspmdrtcontent或gspmdcustomcontent表中,
     * DBO部署后保存在gspdatabaseobject表中
     */
    public static String BEF_ENGINE_2009 = "BEF_ENGINE_2009";
    /**
     * 没有找到字段[{0}]的转换器，请检查字段设置。对应BE[{1}],对象[{2}] 字段[{3}],当前字段类型[{4}],转换后的字段类型[{5}],对应dbo字段类型[{6}]。
     */
    public static String BEF_ENGINE_2010 = "BEF_ENGINE_2010";
    /**
     * 没有找到字段{0},请检查元数据是否部署,对应BE[{1}],对象[{2}]
     */
    public static String BEF_ENGINE_2011 = "BEF_ENGINE_2011";
    /**
     * 业务实体元数据结构错误,版本字段已设置但是未找到，请检查元数据的版本属性设置。对应BE[{0}],对象[{1}],版本字段ID[{2}]
     */
    public static String BEF_ENGINE_2012 = "BEF_ENGINE_2012";
    /**
     * 未找到对象的主键字段，请检查元数据设置。对应BE[{0}], 对象[{1}]
     */
    public static String BEF_ENGINE_2013 = "BEF_ENGINE_2013";
    /**
     * 对象{0}没有设置主键字段，请去元数据设计器中查看,对应BE[{1}]，对象[{2}]
     */
    public static String BEF_ENGINE_2014 = "BEF_ENGINE_2014";
    /**
     * DBO[{0}]没有找到主键对应的数据库字段，请检查dbo是否部署。对应Be[{1}],对象[{2}], dbo[{3}],主键[{4}]
     */
    public static String BEF_ENGINE_2015 = "BEF_ENGINE_2015";
    /**
     * 对象{0}没有设置ParentId字段，请去元数据设计器中查看,对应Be[{1}],对象[{2}]
     */
    public static String BEF_ENGINE_2016 = "BEF_ENGINE_2016";
    /**
     * 没有找到字段[{0}]对应的业务字段，请检查UDT元数据[{1}]是否部署，字段对应BE[{2}],对象[{3}],字段[{4}]
     */
    public static String BEF_ENGINE_2017 = "BEF_ENGINE_2017";
    /**
     * 字段[{0}]关联的模型不存在,请检查关联元数据[{1}]是否部署。字段对应BE[{2}]，对象[{3}],字段[{4}],关联信息[{5}]
     */
    public static String BEF_ENGINE_2018 = "BEF_ENGINE_2018";
    /**
     * 未获取到字段[{0}]关联的SourceElement，请检查关联记录的TargetElement与当前字段ID是否一致或者关联是否记录SourceElement。字段对应BE[{1}],对象[{2}],字段[{3}], 关联的ID:[{4}],关联记录的TargetElement:[{5}]
     */
    public static String BEF_ENGINE_2019 = "BEF_ENGINE_2019";
    /**
     * 没有获取到关联带出字段[{0}]的引用字段，请检查关联元数据[{1}]是否正确部署。带出字段对应BE[{2}],对象[{3}],所属字段[{4}],带出字段[{5}],关联实体{6}
     */
    public static String BEF_ENGINE_2020 = "BEF_ENGINE_2020";
    /**
     * 找不到关联字段[{0}]对应的对象，请检查元数据[{1}]是否正确部署。字段对应BE[{2}],对象[{3}],字段[{4}],关联信息[{5}]
     */
    public static String BEF_ENGINE_2021 = "BEF_ENGINE_2021";
    /**
     * 暂不支持数据库类型
     */
    public static String BEF_ENGINE_2022 = "BEF_ENGINE_2022";
    /**
     * 字段{0}设置的关联为空,对应Be[{1}],对象[{2}]
     */
    public static String BEF_ENGINE_2023 = "BEF_ENGINE_2023";
    /**
     * 字段[{0}]转换为Integer类型失败,值为[{1}],字段对应BE[{2}], 对象[{3}],字段[{4}]
     */
    public static String BEF_ENGINE_2024 = "BEF_ENGINE_2024";
    /**
     * 字段[{0}]转换为BigDecimal类型失败,值为[{1}],字段对应BE[{2}], 对象[{3}],字段[{4}]
     */
    public static String BEF_ENGINE_2025 = "BEF_ENGINE_2025";
    /**
     * 字段[{0}]转换为boolean类型失败,值为{1},字段对应BE[{2}], 对象[{3}],字段[{4}]
     */
    public static String BEF_ENGINE_2026 = "BEF_ENGINE_2026";
    /**
     * 字段{0}转换为Date类型失败,值为{1},字段对应BE[{2}], 对象[{3}],字段[{4}]
     */
    public static String BEF_ENGINE_2027 = "BEF_ENGINE_2027";
    /**
     * 不正确的布尔值{0}
     */
    public static String BEF_ENGINE_2028 = "BEF_ENGINE_2028";
    /**
     * 解析备注类型数据{0}失败
     */
    public static String BEF_ENGINE_2029 = "BEF_ENGINE_2029";
    /**
     * 字段{0}转换为Clob类型失败,值为{1},字段对应BE[{2}], 对象[{3}],字段[{4}]
     */
    public static String BEF_ENGINE_2030 = "BEF_ENGINE_2030";
    /**
     * 没有找到字段[{0}]对应的UDT元数据,请检查UDT元数据[{1}]是否正确部署。字段对应BE[{2}], 对象[{3}],字段[{4}]
     */
    public static String BEF_ENGINE_2031 = "BEF_ENGINE_2031";
    /**
     * 在UDT中没有找到带出字段[{0}]关联的字段，请检查UDT元数据[{1}]是否正确部署。 字段对应BE[{2}]，对象[{3}],所属字段:[{4}],带出字段:[{5}]
     */
    public static String BEF_ENGINE_2032 = "BEF_ENGINE_2032";
    /**
     * udt{0}中未找到id为{1}的字段，请检查UDT[{2}]元数据是否部署。
     */
    public static String BEF_ENGINE_2033 = "BEF_ENGINE_2033";
    /**
     * 枚举字段[{0}]进行数据转换时出错,请输入正确的枚举索引值或编号值，当前传入值为[{1}]。所属BE[{2}]，对象[{3}]，字段[{4}]
     */
    public static String BEF_ENGINE_2034 = "BEF_ENGINE_2034";
    /**
     * 字段{0}存在未加密历史数据，请先检查数据。
     */
    public static String BEF_ENGINE_2035 = "BEF_ENGINE_2035";

    /**
     * 字段[{0}]类型转换出错，对应BE[{1}],对象[{2}],字段[{3}]
     */
    public static String BEF_ENGINE_2036 = "BEF_ENGINE_2036";

    /**
     * 字段[{0}]对应子元素{1}在Udt元数据{2}中未找到，对应BE[{3}],对象[{4}],字段[{5}]
     */
    public static String BEF_ENGINE_2037 = "BEF_ENGINE_2037";

}
