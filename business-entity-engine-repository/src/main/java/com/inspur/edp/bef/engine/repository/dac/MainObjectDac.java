/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.repository.dac;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.engine.repository.adaptor.*;
import com.inspur.edp.cef.api.repository.adaptor.IEntityAdaptor;
import com.inspur.edp.cef.entity.entity.IChildEntityData;
import com.inspur.edp.cef.repository.dac.ChildEntityDac;
import com.inspur.edp.cef.repository.dac.MainEntityDac;
import lombok.var;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainObjectDac extends MainEntityDac {

    private GspBusinessEntity be;
    private GspBizEntityObject mainObj;
    private IEntityAdaptor adaptor;
    private ArrayList<ChildEntityDac> childDac;

    public MainObjectDac(GspBusinessEntity be) {
        this.be = be;
        mainObj = be.getMainObject();
    }

    @Override
    protected List<ChildEntityDac> getChildDacList() {
        if (childDac != null)
            return childDac;
        if (childDac != null)
            return childDac;
        childDac = EngineDacUtils.getChildDacList(mainObj, this);

        return childDac;
    }

    @Override
    public String getNodeCode() {
        return mainObj.getCode();
    }

    @Override
    protected IEntityAdaptor innerGetAdaptor() {
        if (adaptor == null) {
            adaptor = EngineDacUtils.getAdaptor(getDbType(), mainObj, this);
        }
        return adaptor;
    }

    /**
     * 解决IDP推送BE子表关联主表的非主键字段
     *
     * @param map
     */
    protected void handleChildParentId(String dataId, Map<String, List<IChildEntityData>> map) {
        if (map == null || map.size() == 0)
            return;
        String oldKey = "";
        for (Map.Entry<String, List<IChildEntityData>> entry : map.entrySet()) {
            oldKey = entry.getKey();
            break;
        }
        List<IChildEntityData> childEntityDatas = map.get(oldKey);
        map.clear();
        map.put(dataId, childEntityDatas);
    }
}
