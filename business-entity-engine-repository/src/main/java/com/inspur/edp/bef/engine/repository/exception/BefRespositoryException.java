package com.inspur.edp.bef.engine.repository.exception;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

public class BefRespositoryException extends CAFRuntimeException {
    private static final String SU = "pfcommon";
    private static final String RESOURCE_FILE = "bef_engine_repository_exception.properties";

    public BefRespositoryException(String errorcodes, String... messageParams) {
        super(SU, RESOURCE_FILE, errorcodes, messageParams, null, ExceptionLevel.Error, true);
    }

    public BefRespositoryException(String errorcodes, boolean isBizException, String... messageParams) {
        super(SU, RESOURCE_FILE, errorcodes, messageParams, null, ExceptionLevel.Error, isBizException);
    }

    public BefRespositoryException(String errorcodes,  Exception innerException, String... messageParams) {
        super(SU, RESOURCE_FILE, errorcodes, messageParams, innerException, ExceptionLevel.Error, false);
    }

    public BefRespositoryException(String errorcodes,  Exception innerException, boolean isBizException, String... messageParams) {
        super(SU, RESOURCE_FILE, errorcodes, messageParams, innerException, ExceptionLevel.Error, isBizException);
    }
}
