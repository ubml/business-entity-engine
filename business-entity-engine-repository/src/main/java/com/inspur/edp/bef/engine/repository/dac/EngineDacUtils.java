/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.repository.dac;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.engine.repository.adaptor.BefBaseAdaptor;
import com.inspur.edp.bef.engine.repository.adaptor.BefPgAdaptor;
import com.inspur.edp.bef.engine.repository.exception.BefRespositoryException;
import com.inspur.edp.bef.engine.repository.exception.ErrorCodes;
import com.inspur.edp.cef.api.repository.GspDbType;
import com.inspur.edp.cef.api.repository.adaptor.IEntityAdaptor;
import com.inspur.edp.cef.repository.dac.ChildEntityDac;
import com.inspur.edp.cef.repository.dac.EntityDac;
import lombok.var;

import java.util.ArrayList;

final class EngineDacUtils {
    static ArrayList<ChildEntityDac> getChildDacList(GspBizEntityObject bizEntityObject, EntityDac parentDac) {
        ArrayList<ChildEntityDac> childDac = new ArrayList<>();

        if (bizEntityObject.getContainChildObjects() == null || bizEntityObject.getContainChildObjects().size() < 1)
            return childDac;

        for (var childObj : bizEntityObject.getContainChildObjects()) {
            if (childObj.getIsVirtual())
                continue;
            childDac.add(new ChildObjectDac(parentDac, (GspBizEntityObject) childObj));
        }

        return childDac;
    }

    public static IEntityAdaptor getAdaptor(GspDbType dbType, GspBizEntityObject bizEntityObject, EntityDac entityDac) {
        switch (dbType) {
            case PgSQL:
            case OpenGauss:
                return new BefPgAdaptor(bizEntityObject, entityDac, dbType);
            case Unknown:
                throw new BefRespositoryException(ErrorCodes.BEF_ENGINE_2022);
            default:
                return new BefBaseAdaptor(bizEntityObject, entityDac, dbType);
        }
    }
}
