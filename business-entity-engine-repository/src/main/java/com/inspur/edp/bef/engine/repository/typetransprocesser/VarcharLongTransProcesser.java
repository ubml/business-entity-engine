package com.inspur.edp.bef.engine.repository.typetransprocesser;

import com.inspur.edp.cef.api.repository.ITypeTransProcesser;
import com.inspur.edp.cef.entity.condition.FilterCondition;

import java.sql.Connection;

/**
 * @author Kaixuan Shi
 * @since 2024/2/23
 */
public class VarcharLongTransProcesser implements ITypeTransProcesser {

    private static volatile VarcharLongTransProcesser instance;

    private VarcharLongTransProcesser() {
    }

    public static VarcharLongTransProcesser getInstance() {
        if (instance == null) {
            instance = new VarcharLongTransProcesser();
        }
        return instance;
    }

    @Override
    public Object transType(FilterCondition condition, Connection connection) {
        return this.transType(condition.getValue());
    }

    @Override
    public Object transType(Object value) {
        return this.transType(value, true);
    }

    @Override
    public Object transType(Object value, boolean isNull) {

        if (value == null || "".equals(value)) {
            return null;
        } else if (value instanceof Long) {
            return value;
        } else if (value instanceof String) {
            return Long.parseLong(value.toString());
        }
        //关联对象会被直接传进来，原样返回
        return value;
    }
}
