/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.repository.dac;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.engine.repository.adaptor.*;
import com.inspur.edp.cef.api.repository.adaptor.IEntityAdaptor;
import com.inspur.edp.cef.repository.dac.ChildEntityDac;
import com.inspur.edp.cef.repository.dac.EntityDac;
import lombok.var;

import java.util.ArrayList;
import java.util.List;

public class ChildObjectDac extends ChildEntityDac {

    private GspBizEntityObject bizEntityObject;
    private ArrayList<ChildEntityDac> childDac;
    private IEntityAdaptor adaptor;

    public ChildObjectDac(EntityDac parentDac, GspBizEntityObject bizEntityObject) {
        super(parentDac);
        this.bizEntityObject = bizEntityObject;
    }

    @Override
    protected List<ChildEntityDac> getChildDacList() {
        if (childDac != null)
            return childDac;
        childDac = EngineDacUtils.getChildDacList(bizEntityObject, this);

        return childDac;
    }

    @Override
    public String getNodeCode() {
        return bizEntityObject.getCode();
    }

    @Override
    protected IEntityAdaptor innerGetAdaptor() {
        if (adaptor == null) {
            adaptor = EngineDacUtils.getAdaptor(getDbType(), bizEntityObject, this);

        }
        return adaptor;
    }
}
