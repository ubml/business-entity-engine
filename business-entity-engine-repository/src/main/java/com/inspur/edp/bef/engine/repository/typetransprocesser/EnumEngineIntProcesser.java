/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.repository.typetransprocesser;

import com.inspur.edp.bef.bizentity.GspBizEntityElement;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.engine.repository.util.ExceptionUtil;
import com.inspur.edp.cef.api.repository.ITypeTransProcesser;
import com.inspur.edp.cef.designtime.api.element.ElementDefaultVauleType;
import com.inspur.edp.cef.designtime.api.element.EnumIndexType;
import com.inspur.edp.cef.designtime.api.element.GspEnumValue;
import com.inspur.edp.cef.entity.condition.FilterCondition;
import com.inspur.edp.bef.engine.repository.exception.BefRespositoryException;
import com.inspur.edp.bef.engine.repository.exception.ErrorCodes;
import com.inspur.edp.cef.repository.exception.CefRepositoryException;
import com.inspur.edp.cef.repository.utils.FilterUtil;
import com.inspur.edp.das.commonmodel.IGspCommonModel;
import com.inspur.edp.das.commonmodel.IGspCommonObject;

import java.sql.Connection;

public class EnumEngineIntProcesser implements ITypeTransProcesser {
    private GspBizEntityElement element;

    public EnumEngineIntProcesser(GspBizEntityElement element) {
        this.element = element;
    }

    @Override
    public Object transType(FilterCondition condition, Connection connection) {

        return transType(FilterUtil.processStringValue(condition));
    }

    @Override
    public Object transType(Object o) {

        if (o == null) {
            return null;
        } else if (o instanceof String) {
            if ("".equals(o))
                return "";
            return getIndex(element, (String) o);

        } else if (o instanceof Integer) {
            return (Integer) o;
        } else {
            throw new BefRespositoryException(ErrorCodes.BEF_ENGINE_2001, false, element.getLabelID(), o.toString(), o.getClass().getTypeName(),
                    ExceptionUtil.getBEInfo(element.getBelongObject()), ExceptionUtil.getBizEntityObjInfo(element.getBelongObject()),
                    ExceptionUtil.getBizElementInfo(element));
        }
    }

    @Override
    public Object transType(Object o, boolean b) {
        return transType(o);
    }

    /**
     * 将传入值转换为整数枚举的持久化值(index索引)
     * @param element
     * @param value
     * @return
     */
    private Integer getIndex(GspBizEntityElement element, String value) {
        for (GspEnumValue enumValue : element.getContainEnumValues()) {
            //判断传进去的参数是枚举索引值(对应持久化值)
            String enumIndex = String.valueOf(enumValue.getIndex());
            if (value.equals(enumIndex)) {
                return enumValue.getIndex();
            }
            //判断传进来的参数是枚举编号(现在对应的是内存值)
            String enumCode = enumValue.getValue();
            if (value.equals(enumCode)) {
                return enumValue.getIndex();
            }
        }
        throw new BefRespositoryException(ErrorCodes.BEF_ENGINE_2034, false,
                element.getLabelID(),
                value,
                ExceptionUtil.getBEInfo(element.getBelongObject()),
                ExceptionUtil.getBizEntityObjInfo(element.getBelongObject()),
                ExceptionUtil.getBizElementInfo(element));
    }
}
