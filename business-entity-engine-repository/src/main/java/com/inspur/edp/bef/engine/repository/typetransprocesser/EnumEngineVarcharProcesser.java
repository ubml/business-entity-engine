/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.repository.typetransprocesser;

import com.inspur.edp.bef.bizentity.GspBizEntityElement;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.engine.repository.util.ExceptionUtil;
import com.inspur.edp.cef.api.repository.ITypeTransProcesser;
import com.inspur.edp.cef.designtime.api.element.EnumIndexType;
import com.inspur.edp.cef.designtime.api.element.GspEnumValue;
import com.inspur.edp.cef.entity.condition.FilterCondition;
import com.inspur.edp.bef.engine.repository.exception.BefRespositoryException;
import com.inspur.edp.bef.engine.repository.exception.ErrorCodes;
import com.inspur.edp.cef.repository.exception.CefRepositoryException;
import com.inspur.edp.cef.repository.utils.FilterUtil;
import com.inspur.edp.das.commonmodel.IGspCommonModel;
import com.inspur.edp.das.commonmodel.IGspCommonObject;

import java.sql.Connection;

public class EnumEngineVarcharProcesser implements ITypeTransProcesser {
    private GspBizEntityElement element;

    public EnumEngineVarcharProcesser(GspBizEntityElement element) {
        this.element = element;
    }

    @Override
    public Object transType(FilterCondition condition, Connection connection) {
        return transType(FilterUtil.processStringValue(condition));
    }

    /**
     * 将传入值转化为枚举持久化值 ?()
     * @param o
     * @return
     */
    @Override
    public Object transType(Object o) {
        if (o == null) {
            return null;
        } else if (o instanceof String) {
            if ("".equals(o))
                return "";
            return getIndex(element, (String) o);
        } else if (o instanceof Integer) {
            //todo 有个隐患，如果传入的是Integer，没有校验数据的合法性
            return (Integer) o;
        } else {
            throw new BefRespositoryException(ErrorCodes.BEF_ENGINE_2001, false, element.getLabelID(), o.toString(), o.getClass().getTypeName(),
                    ExceptionUtil.getBEInfo(element.getBelongObject()), ExceptionUtil.getBizEntityObjInfo(element.getBelongObject()),
                    ExceptionUtil.getBizElementInfo(element));
        }
    }

    private Object getIndex(GspBizEntityElement element, String value) {
        switch (element.getEnumIndexType()) {
            case Integer:
                return getIntIndex(element, value);
            case String:
                return getStringIndex(element, value);
        }
        throw new BefRespositoryException(ErrorCodes.BEF_ENGINE_2002, element.getLabelID(), element.getEnumIndexType().toString());
    }

    /**
     * 获取枚举持久化值
     * @param element
     * @param value
     * @return
     */
    private Integer getIntIndex(GspBizEntityElement element, String value) {
        for (GspEnumValue enumValue : element.getContainEnumValues()) {

            //判断传进去的参数是枚举索引值
            String enumIndex = String.valueOf(enumValue.getIndex());
            if (value.equals(enumIndex)) {
                return enumValue.getIndex();
            }
            //判断传进来的参数是枚举编号
            String enumCode = enumValue.getValue();
            if (value.equals(enumCode)) {
                return enumValue.getIndex();
            }
        }
        throw new BefRespositoryException(ErrorCodes.BEF_ENGINE_2034, false,
                element.getLabelID(),
                value,
                ExceptionUtil.getBEInfo(element.getBelongObject()),
                ExceptionUtil.getBizEntityObjInfo(element.getBelongObject()),
                ExceptionUtil.getBizElementInfo(element));
    }

    private String getStringIndex(GspBizEntityElement element, String value) {
        for (GspEnumValue enumValue : element.getContainEnumValues()) {

            //判断传进去的参数是枚举索引值
            String enumIndex = enumValue.getStringIndex();
            if (value.equals(enumIndex)) {
                return enumValue.getStringIndex();
            }
            //判断传进来的参数是枚举编号
            String enumCode = enumValue.getValue();
            if (value.equals(enumCode)) {
                return enumValue.getStringIndex();
            }
        }
        throw new BefRespositoryException(ErrorCodes.BEF_ENGINE_2034, false,
                element.getLabelID(),
                value,
                ExceptionUtil.getBEInfo(element.getBelongObject()),
                ExceptionUtil.getBizEntityObjInfo(element.getBelongObject()),
                ExceptionUtil.getBizElementInfo(element));
    }

    @Override
    public Object transType(Object o, boolean b) {
        return transType(o);
    }
}
