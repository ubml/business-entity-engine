/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.entity.propertyinfobuilder.cef;

import com.inspur.edp.bef.engine.entity.exception.BefEngineExceptionBase;
import com.inspur.edp.bef.engine.entity.exception.ErrorCodes;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.BasePropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.DynamicSetPropertyInfo;
import org.springframework.util.StringUtils;

public class DynamicPropInfoBuilder extends BasePropertyInfoBuilder {

    public DynamicPropInfoBuilder(IGspCommonField field,
                                  CefPropInfoGenerator propInfoGenerator) {
        super(field, propInfoGenerator);
    }

    @Override
    public final BasePropertyInfo getBasePropertyInfo(IGspCommonField field) {
        if (field.getDynamicPropSetInfo() == null
                || field.getDynamicPropSetInfo().getDynamicPropSerializerComp() == null || StringUtils
                .isEmpty(field.getDynamicPropSetInfo().getDynamicPropSerializerComp().getId())) {
            throw new BefEngineExceptionBase(ErrorCodes.BEF_ENGINE_0002, field.getCode());
        }
        return new DynamicSetPropertyInfo(
                field.getDynamicPropSetInfo().getDynamicPropSerializerComp().getId());
    }
}
