/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.entity.propertyinfobuilder.cef;

import com.inspur.edp.bef.engine.entity.propertyinfobuilder.BefPropertyInfoBuilderFactory;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.DataTypePropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.RefDataTypePropertyInfo;

public class CefPropInfoGenerator {
    public CefPropInfoGenerator() {
    }

    public DataTypePropertyInfo getDataTypePropertyInfo(IGspCommonField field) {
        BasePropertyInfoBuilder builder = new BefPropertyInfoBuilderFactory().getBuilder(field, this);
        return new DataTypePropertyInfo(field.getLabelID(), field.getI18nResourceInfoPrefix() + ".Name",
                field.getIsRequire(),
                field.isEnableRtrim(), field.getLength(), field.getPrecision(), builder.getFieldType(field),
                builder.getObjectType(field),
                builder.getBasePropertyInfo(field), builder.getPropertyDefaultValue(field), field.getIsMultiLanguage(), field.isBigNumber());
    }

    public RefDataTypePropertyInfo getAssociationRefPropertyInfo(IGspCommonField field, IGspCommonField refedField) {
        BasePropertyInfoBuilder builder = new BefPropertyInfoBuilderFactory().getBuilder(field, this);
        return new RefDataTypePropertyInfo(field.getLabelID(), field.getI18nResourceInfoPrefix() + ".Name",
                field.getIsRequire(),
                field.isEnableRtrim(), field.getLength(), field.getPrecision(), builder.getFieldType(field),
                builder.getObjectType(field),
                builder.getBasePropertyInfo(field), builder.getPropertyDefaultValue(field), field.getIsMultiLanguage(), field.isBigNumber(), refedField.getLabelID());
    }
}
