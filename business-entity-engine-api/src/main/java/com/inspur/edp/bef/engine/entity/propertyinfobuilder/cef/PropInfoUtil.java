/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.entity.propertyinfobuilder.cef;

import com.inspur.edp.cef.api.repository.GspDbDataType;
import io.iec.edp.caf.databaseobject.api.entity.DataType;

public class PropInfoUtil {
    public static GspDbDataType transDataType(DataType dataType) {
        GspDbDataType dbDataType = GspDbDataType.VarChar;
        switch (dataType) {
            case Char:
                dbDataType = GspDbDataType.Char;
                break;
            case Boolean:
                dbDataType = GspDbDataType.Boolean;
                break;
            case Varchar:
                dbDataType = GspDbDataType.VarChar;
                break;
            case Blob:
                dbDataType = GspDbDataType.Blob;
                break;
            case DateTime:
                dbDataType = GspDbDataType.DateTime;
                break;
            case TimeStamp:
                dbDataType = GspDbDataType.DateTime;
                break;
            case Clob:
                dbDataType = GspDbDataType.Clob;
                break;
            case Int:
            case SmallInt:
                dbDataType = GspDbDataType.Int;
                break;
            case LongInt:
                dbDataType = GspDbDataType.Long;
                break;
            case Decimal:
                dbDataType = GspDbDataType.Decimal;
                break;
            case NChar:
                dbDataType = GspDbDataType.NChar;
                break;
            case NVarchar:
                dbDataType = GspDbDataType.NVarChar;
                break;
            case NClob:
                dbDataType = GspDbDataType.NClob;
                break;
        }
        return dbDataType;
    }
}
