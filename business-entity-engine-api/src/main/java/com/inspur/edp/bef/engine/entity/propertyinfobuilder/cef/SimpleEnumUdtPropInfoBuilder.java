/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.entity.propertyinfobuilder.cef;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.EnumIndexType;
import com.inspur.edp.cef.designtime.api.element.GspEnumValue;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.EnumPropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.EnumPropertyRepoType;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.SimpleEnumUdtPropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.UdtPropertyInfo;

public class SimpleEnumUdtPropInfoBuilder extends SimpleUdtPropInfoBuilder {

    public SimpleEnumUdtPropInfoBuilder(IGspCommonField field,
                                        CefPropInfoGenerator propInfoGenerator, boolean enableStdTimeFormat) {
        super(field, propInfoGenerator, enableStdTimeFormat);
    }

    @Override
    protected UdtPropertyInfo getUdtPropertyInfo(IGspCommonField field) {
        SimpleEnumUdtPropertyInfo simpleEnumUdtPropertyInfo = new SimpleEnumUdtPropertyInfo(
                getUdtConfigId(), enableStdTimeFormat);
        simpleEnumUdtPropertyInfo.setEnumInfo(getEnumPropertyInfo(field));
        return simpleEnumUdtPropertyInfo;
    }

    public static EngineEnumPropertyInfo getEnumPropertyInfo(IGspCommonField field) {
        EngineEnumPropertyInfo enumPropertyInfo = new EngineEnumPropertyInfo();
        for (GspEnumValue enumValue : field.getContainEnumValues()) {
            enumPropertyInfo
                    .addEnumValueInfo(enumValue.getI18nResourceInfoPrefix() + ".DisplayValue", enumValue.getValue(),
                            enumValue.getName(),
                            enumValue.getStringIndex() == null ? "" : enumValue.getStringIndex(),
                            enumValue.getIndex(),
                            enumValue.getEnumItemDisabled());
        }
        return enumPropertyInfo;
    }
}
