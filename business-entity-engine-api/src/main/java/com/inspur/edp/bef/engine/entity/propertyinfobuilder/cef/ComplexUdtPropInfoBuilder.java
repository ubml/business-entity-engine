/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.entity.propertyinfobuilder.cef;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.ComplexUdtPropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.UdtPropertyInfo;
import com.inspur.edp.commonmodel.engine.api.common.CMEngineUtil;
import com.inspur.edp.udt.designtime.api.entity.ComplexDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;

public class ComplexUdtPropInfoBuilder extends UdtPropertyInfoBuilder {

    public ComplexUdtPropInfoBuilder(IGspCommonField field,
                                     CefPropInfoGenerator propInfoGenerator, boolean enableStdTimeFormat) {
        super(field, propInfoGenerator, enableStdTimeFormat);
    }

    @Override
    protected UdtPropertyInfo getUdtPropertyInfo(IGspCommonField field) {
        ComplexUdtPropertyInfo complexUdtPropertyInfo = new ComplexUdtPropertyInfo(getUdtConfigId(),
                enableStdTimeFormat);
        ComplexDataTypeDef unifiedDataTypeDef = CMEngineUtil.getMetadataContent(field.getUdtID());

        for (IGspCommonField childField : field.getChildElements()) {
            String i18nResourceName = getI18nResourceName(childField);
            String udtPropName = getrefField(field, childField, unifiedDataTypeDef);
            complexUdtPropertyInfo.addPropertyInfo(childField.getLabelID(), i18nResourceName, udtPropName);
        }
        return complexUdtPropertyInfo;
    }

    private String getI18nResourceName(IGspCommonField childElement) {
        if (childElement.getI18nResourceInfoPrefix() == null || childElement.getI18nResourceInfoPrefix().equals("")) {
            return "";
        } else {
            return childElement.getI18nResourceInfoPrefix() + ".Name";
        }
    }

    private String getrefField(IGspCommonField field, IGspCommonField childElement, ComplexDataTypeDef udt) {
        String udtPropName = "";
        IGspCommonField refField = udt
                .findElement(field.getMappingRelation().getMappingInfo(childElement.getID()));

        if (field.getMappingRelation().getMappingInfo(childElement.getID()) != null && field
                .getMappingRelation().getMappingInfo(childElement.getID()).equals(udt.getId())) {
            udtPropName = udt.getCode();
        } else if (refField == null) {
            udtPropName = udt.getCode();
        } else {
            udtPropName = refField.getLabelID();
        }
        return udtPropName;
    }
}
