/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.entity.propertyinfobuilder.cef;

import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.entity.entity.AssoInfoBase;
import com.inspur.edp.cef.spi.entity.AssociationInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.AssocationPropertyInfo;

public class EngineAssoPropertyInfo extends AssocationPropertyInfo {

    private final GspAssociation association;

    public EngineAssoPropertyInfo(AssociationInfo associationInfo, GspAssociation association) {
        super(associationInfo);
        this.association = association;
    }

    @Override
    protected AssoInfoBase createNewInstance() {
        return new com.inspur.edp.bef.engine.entity.AssociationInfo(association);
    }
}
