/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.entity.propertyinfobuilder.cef;

import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.common.BefDtBeanUtil;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.BasePropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.UdtPropertyInfo;
import com.inspur.edp.commonmodel.engine.api.common.CMEngineUtil;
import com.inspur.edp.metadata.rtcustomization.api.CustomizationRtService;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;

public abstract class UdtPropertyInfoBuilder extends BasePropertyInfoBuilder {

    protected final boolean enableStdTimeFormat;

    public UdtPropertyInfoBuilder(IGspCommonField field, CefPropInfoGenerator propInfoGenerator,
                                  boolean enableStdTimeFormat) {
        super(field, propInfoGenerator);
        this.enableStdTimeFormat = enableStdTimeFormat;
    }

    @Override
    public final BasePropertyInfo getBasePropertyInfo(IGspCommonField field) {
        return getUdtPropertyInfo(field);
    }

    protected abstract UdtPropertyInfo getUdtPropertyInfo(IGspCommonField field);

    protected String getUdtConfigId() {
        UnifiedDataTypeDef udt = CMEngineUtil.getMetadataContent(field.getUdtID());
        return udt.getUdtType();
    }
}
