package com.inspur.edp.bef.engine.entity.exception;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

public class BefEngineExceptionBase extends CAFRuntimeException {
    private static final String serviceUnitCode = "pfcommon";
    public static final String RESOURCE_FILE = "bef_engine_api_exception.properties";

    public BefEngineExceptionBase(Exception e, String errorCodes) {
        super(serviceUnitCode, RESOURCE_FILE, errorCodes, null, e, ExceptionLevel.Error, true);
    }

    public BefEngineExceptionBase(String errorCodes, String... messageParams) {
        super(serviceUnitCode, RESOURCE_FILE, errorCodes, messageParams, null, ExceptionLevel.Error, true);
    }

}


