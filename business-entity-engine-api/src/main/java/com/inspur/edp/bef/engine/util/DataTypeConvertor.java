package com.inspur.edp.bef.engine.util;

import com.inspur.edp.cef.api.repository.GspDbDataType;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.dataaccess.DbType;
import io.iec.edp.caf.databaseobject.api.entity.DataType;

/**
 * @className: 数据类型转换器，用与dbo 数据类型与bef数据类型转换
 * @author: wangmj
 * @date: 2024/8/8
 **/
public class DataTypeConvertor {
    /**
     * 将CAFDataType转化为BEF的GspDbDataType类型
     * @param dataType
     * @return
     */
    public static GspDbDataType transDataType(DataType dataType) {
        GspDbDataType dbDataType = GspDbDataType.VarChar;
        switch (dataType) {
            case Char:
                dbDataType = GspDbDataType.Char;
                break;
            case Varchar:
                dbDataType = GspDbDataType.VarChar;
                break;
            case Blob:
                dbDataType = GspDbDataType.Blob;
                break;
            case DateTime: {
                //慢慢的都改过来。
                if (CAFContext.current.getDbType() == DbType.OceanBase) {
                    dbDataType = GspDbDataType.Date;
                } else {
                    dbDataType = GspDbDataType.DateTime;
                }
                break;
            }
            case TimeStamp:
                dbDataType = GspDbDataType.DateTime;
                break;
            case Clob:
                dbDataType = GspDbDataType.Clob;
                break;
            case SmallInt:
                dbDataType = GspDbDataType.SmallInt;
                break;
            case Int:
                dbDataType = GspDbDataType.Int;
                break;
            case LongInt:
                dbDataType = GspDbDataType.Long;
                break;
            case Decimal:
            case Float:
                dbDataType = GspDbDataType.Decimal;
                break;
            case NChar:
                dbDataType = GspDbDataType.NChar;
                break;
            case NVarchar:
                dbDataType = GspDbDataType.NVarChar;
                break;
            case NClob:
                dbDataType = GspDbDataType.NClob;
                break;
            case Boolean:
                dbDataType = GspDbDataType.Boolean;
                break;
            case Jsonb:
                dbDataType = GspDbDataType.Jsonb;
                break;
            default:
            {
                //todo 添加日志输出？
                break;
            }
        }
        return dbDataType;
    }
}
