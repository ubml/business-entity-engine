package com.inspur.edp.bef.engine.entity.exception;

public class ErrorCodes {

    /**
     * 对象类型不存在
     */
    public static String BEF_ENGINE_0001 = "BEF_ENGINE_0001";
    /**
     * 字段{0}上未设置序列化构件
     * 0:field.getCode()
     */
    public static String BEF_ENGINE_0002 = "BEF_ENGINE_0002";
    /**
     * 节点{0}上字段{1}的关联信息不存在, 请修改
     * 0:节点 1:字段
     */
    public static String BEF_ENGINE_0003 = "BEF_ENGINE_0003";
    /**
     * 节点{0}上的字段{1}关联的ID,名称分别为{2},{3}的业务实体元数据获取不到(调用接口为com.inspur.edp.lcm.metadata.api.service.MetadataRTService.getMetadata(MetadataCustomizationFilter)),"
     * + "请联系被关联业务实体的开发人员检查是否正确部署, 通常正确部署后gspmdrtcontent表中应存在此元数据相关信息
     * 0:节点  1:字段 2:业务实体元数据名称 3:业务实体元数据名称
     */
    public static String BEF_ENGINE_0004 = "BEF_ENGINE_0004";
    /**
     * 关联模型{2}上未获取到ID为{1}，编号为{0}的节点对象,请检查确认元数据是否修改或是否正确部署
     * 0:节点对象编号 1:节点对象ID 2:关联模型
     */
    public static String BEF_ENGINE_0005 = "BEF_ENGINE_0005";
    /**
     * "检查字段'"+association.getBelongElement().getName()+"'的关联带出字段'"+refField.getName()+"'时出错，在被关联的业务实体'"+refEntity.getName()+"'中找不到该字段。
     * 检查字段{0}的关联带出字段{1}出错，在被关联的业务实体{2}中找不到该字段
     */
    public static String BEF_ENGINE_0006 = "BEF_ENGINE_0006";
    /**
     * 未找到id为{0}，名称为{1}的UDT元数据
     * 0:UDT元数据id 1:UDT元数据名称
     */
    public static String BEF_ENGINE_0007 = "BEF_ENGINE_0007";
    /**
     * 单值udt中不支持设置动态属性
     */
    public static String BEF_ENGINE_0008 = "BEF_ENGINE_0008";
    /**
     * befenginequerylistener实例初始化失败
     */
    public static String BEF_ENGINE_0009 = "BEF_ENGINE_0009";
    /**
     * 节点{0}中{1}对应的字段{2}未找到
     */
    public static String BEF_ENGINE_0010 = "BEF_ENGINE_0010";
    /**
     * VO字段{0}对应的BE{1}上ID为{2}，编号为{3}的字段关联带出字段集合为空
     */
    public static String BEF_ENGINE_0011 = "BEF_ENGINE_0011";
    /**
     * 未获取到实体{0}上ID为{1}的字段，请确认BE元数据是否部署或者dbo是否被覆盖修改
     */
    public static String BEF_ENGINE_0012 = "BEF_ENGINE_0012";
    /**
     * 模型{0}上的对象{1}对应的dbo没有获取到，请检查dbo是否成功部署到正确的数据库中，dboId为{2}
     */
    public static String BEF_ENGINE_0013 = "BEF_ENGINE_0013";


}
