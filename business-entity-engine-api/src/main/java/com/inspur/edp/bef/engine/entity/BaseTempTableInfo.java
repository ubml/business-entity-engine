/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.entity;

import com.inspur.edp.bef.bizentity.GspBizEntityElement;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.cef.designtime.api.element.GspElementDataType;
import com.inspur.edp.cef.designtime.api.entity.GspCommonField;
import com.inspur.edp.das.commonmodel.entity.object.GspColumnGenerate;

import java.util.ArrayList;

public class BaseTempTableInfo {
    private java.util.List<TempColumnInfo> columns = new ArrayList<>();
    GspBusinessEntity gspBusinessEntity = new GspBusinessEntity();
    GspBizEntityObject gspBizEntityObject = new GspBizEntityObject();

    public BaseTempTableInfo(String code, String name, String beId) {
        this.code = code;
        this.name = name;
        this.beId = beId;
        gspBusinessEntity.setID(this.beId);
        gspBusinessEntity.setCode(this.code);
        gspBusinessEntity.setName(this.name);

        gspBizEntityObject.setCode(this.code);
        gspBizEntityObject.setName(this.name);
    }

    private String code = "";
    private String name = "";
    private String beId = "";

    public String getCode() {
        return this.code;
    }

    public void setCode(String value) {
        this.code = value;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public void addColumn(String columnName, GspElementDataType gspElementDataType, boolean isPrimaryKey, boolean isRequire, boolean isNull) {
        addColumn(columnName, gspElementDataType, isPrimaryKey, isRequire, isNull, 0, 0);
    }

    public void addColumn(String columnName, GspElementDataType gspElementDataType, boolean isPrimaryKey, boolean isRequire, boolean isNull, int length, int precision) {
        TempColumnInfo tempColumnInfo = new TempColumnInfo(columnName, gspElementDataType, isPrimaryKey, isRequire, isNull, length, precision);
        columns.add(tempColumnInfo);
    }

    public void initBusinessEntity() {
        for (TempColumnInfo columnInfo : columns) {
            GspBizEntityElement gspBizEntityElement = new GspBizEntityElement();
            gspBizEntityElement.setID(java.util.UUID.randomUUID().toString());
            gspBizEntityElement.setLabelID(columnInfo.getColunmName());
            gspBizEntityElement.setMDataType(columnInfo.getGspElementDataType());
            gspBizEntityObject.getContainElements().add(gspBizEntityElement);
            if (columnInfo.isPrimaryKey()) {
                GspColumnGenerate gspColumnGenerate = new GspColumnGenerate();
                gspColumnGenerate.setElementID(gspBizEntityElement.getID());
                gspColumnGenerate.setGenerateType("Guid");
                gspBizEntityObject.setColumnGenerateID(gspColumnGenerate);
            }
            gspBizEntityElement.setLength(columnInfo.getLength());
            gspBizEntityElement.setPrecision(columnInfo.getPrecision());
//            gspBizEntityElement.setPrecision(columnInfo.getPrecision());
        }
        gspBusinessEntity.setMainObject(gspBizEntityObject);
    }

    public GspBusinessEntity getBusinessEntity() {
        return gspBusinessEntity;
    }

    public GspBizEntityObject getBizEntityObject() {
        return gspBizEntityObject;
    }
}

class TempColumnInfo {
    private String colunmName;

    public String getColunmName() {
        return colunmName;
    }

    public void setColunmName(String colunmName) {
        this.colunmName = colunmName;
    }

    public GspElementDataType getGspElementDataType() {
        return gspElementDataType;
    }

    public void setGspElementDataType(GspElementDataType gspElementDataType) {
        this.gspElementDataType = gspElementDataType;
    }

    public boolean isPrimaryKey() {
        return isPrimaryKey;
    }

    public void setPrimaryKey(boolean primaryKey) {
        isPrimaryKey = primaryKey;
    }

    public boolean isRequire() {
        return isRequire;
    }

    public void setRequire(boolean require) {
        isRequire = require;
    }

    public boolean isNull() {
        return isNull;
    }

    public void setNull(boolean aNull) {
        isNull = aNull;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getPrecision() {
        return precision;
    }

    public void setPrecision(int precision) {
        this.precision = precision;
    }

    private GspElementDataType gspElementDataType;//be字段类型

    public TempColumnInfo(String colunmName, GspElementDataType gspElementDataType, boolean isPrimaryKey, boolean isRequire, boolean isNull, int length, int precision) {
        this.colunmName = colunmName;
        this.gspElementDataType = gspElementDataType;
        this.isPrimaryKey = isPrimaryKey;
        this.isRequire = isRequire;
        this.isNull = isNull;
        this.length = length;
        this.precision = precision;
    }

    private boolean isPrimaryKey;//是否主键
    private boolean isRequire;//是否必填
    private boolean isNull;//是否可为空
    private int length;//长度
    private int precision;//精度
}
