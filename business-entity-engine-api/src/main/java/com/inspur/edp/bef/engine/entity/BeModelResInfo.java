/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.entity;

import com.inspur.edp.bef.api.be.BefLockType;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.beenum.GspDataLockType;
import com.inspur.edp.bef.spi.entity.builtinimpls.BefModelResInfoImpl;
import com.inspur.edp.cef.spi.entity.resourceInfo.EntityResInfo;
import com.inspur.edp.cef.spi.entity.resourceInfo.ModelResInfo;
import com.inspur.edp.cef.spi.jsonser.base.StringUtils;
import com.inspur.edp.commonmodel.engine.api.common.CMEngineUtil;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.MetadataReference;
import io.iec.edp.caf.businessobject.api.entity.DevBasicBoInfo;
import io.iec.edp.caf.businessobject.api.service.DevBasicInfoService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.util.ArrayList;

public class BeModelResInfo extends BefModelResInfoImpl {
    private final GspBusinessEntity be;
    private String suCode;
    private String resMetaDatId;

    public BeModelResInfo(GspBusinessEntity be, String su, String resMetadataId) {
        super(su, resMetadataId, be.getID(), be.getI18nResourceInfoPrefix() + ".Name", getLockType(be));
        this.be = be;
        setRootEntityResInfo(new EntityResourceInfo(be.getMainObject(), this, be));

        setTccSupported(be.isTccSupported());
        setAutoCancel(be.isAutoCancel());
        setAutoConfirm(be.isAutoComplete());
        setAutoCancel(be.isAutoCancel());
    }

    private static BefLockType getLockType(GspBusinessEntity be1) {
        if (be1.getDataLockType() != GspDataLockType.None)
            return BefLockType.distributeLock;
        return BefLockType.None;
    }

    public String getConfigId() {
        return this.be.getGeneratedConfigID();
    }

    //    @Override
    public String getCurrentSu() {
        if (StringUtils.isEmpty(this.suCode)) {
            GspMetadata metadata = CMEngineUtil.getMetadata(be.getId());
            DevBasicInfoService devBasicInfoService = SpringBeanUtils.getBean(DevBasicInfoService.class);
            if (metadata.getRefs() != null) {
                for (MetadataReference reference : metadata.getRefs()) {
                    if (reference.getDependentMetadata().getType().equals("ResourceMetadata")) {
                        this.resMetaDatId = reference.getDependentMetadata().getId();
                        break;
                    }
                }
            }

            DevBasicBoInfo devBasicBoInfo = null;
            if (!StringUtils.isEmpty(metadata.getHeader().getBizobjectID())) {
                devBasicBoInfo = devBasicInfoService.getDevBasicBoInfo(metadata.getHeader().getBizobjectID());
            }

            if (devBasicBoInfo != null) {
                this.suCode = devBasicBoInfo.getSuCode();
            }
        }
        return this.suCode;
    }

    @Override
    public String getResourceMetaId() {
        if (StringUtils.isEmpty(this.resMetaDatId)) {
            GspMetadata metadata = CMEngineUtil.getMetadata(be.getId());
            DevBasicInfoService devBasicInfoService = SpringBeanUtils.getBean(DevBasicInfoService.class);
            if (metadata.getRefs() != null) {
                for (MetadataReference reference : metadata.getRefs()) {
                    if (reference.getDependentMetadata().getType().equals("ResourceMetadata")) {
                        this.resMetaDatId = reference.getDependentMetadata().getId();
                        break;
                    }
                }
            }
        }
        return this.resMetaDatId;
    }
}
