/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.entity.propertyinfobuilder.cef;

import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.common.BefDtBeanUtil;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.AssocationPropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.SimpleAssoUdtPropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.UdtPropertyInfo;
import com.inspur.edp.commonmodel.engine.api.common.CMEngineUtil;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.entity.GspCommonModel;
import com.inspur.edp.metadata.rtcustomization.api.CustomizationRtService;
import com.inspur.edp.udt.designtime.api.entity.ComplexDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.SimpleDataTypeDef;

public class SimpleAssoUdtPropInfoBuilder extends SimpleUdtPropInfoBuilder {

    GspBusinessEntity refEntity = null;

    public SimpleAssoUdtPropInfoBuilder(IGspCommonField field,
                                        CefPropInfoGenerator propInfoGenerator, boolean enableStdTimeFormat) {
        super(field, propInfoGenerator, enableStdTimeFormat);
    }

    private GspBusinessEntity getRefEntity() {
        if (refEntity == null) {
            refEntity = CMEngineUtil.getMetadataContent(getAssociation().getRefModelID());
        }
        return refEntity;
    }

    private GspAssociation getAssociation() {
        if (field.getIsRefElement() == false)
            return field.getChildAssociations().get(0);

        String refModelId = field.getParentAssociation().getRefModelID();
        CustomizationRtService mdService = BefDtBeanUtil.getCustomizationRtService();
        GspBusinessEntity refEntity = CMEngineUtil.getMetadataContent(refModelId);
        return refEntity.findElementById(field.getRefElementId()).getChildAssociations().get(0);
    }

    @Override
    protected UdtPropertyInfo getUdtPropertyInfo(IGspCommonField field) {
        SimpleAssoUdtPropertyInfo simpleAssoUdtPropertyInfo = new SimpleAssoUdtPropertyInfo(
                getUdtConfigId(), enableStdTimeFormat);

        AssocationPropertyInfo assocationPropertyInfo = AssoPropInfoBuilder.getAssoPropertyInfo(field, getAssociation(), getRefEntity(), propInfoGenerator);
        simpleAssoUdtPropertyInfo.setAssoInfo(assocationPropertyInfo);
        dealMapping(simpleAssoUdtPropertyInfo);
        return simpleAssoUdtPropertyInfo;
    }

    private void dealMapping(SimpleAssoUdtPropertyInfo simpleAssoUdtPropertyInfo) {
        GspAssociation association = getAssociation();
        for (IGspCommonField refEle : association.getRefElementCollection()) {
//          if( field instanceof IGspCommonElement && field.getIsUdt() && !refEle.getIsFromAssoUdt())
//              return;
            String refPropName = refEle.getLabelID();
            String refUdtPropName = getRefedUdtPropertyName(refEle);
            String referedPropName = getRefedPropertyName(refEle);
            simpleAssoUdtPropertyInfo.addRefProperty(refPropName, refUdtPropName, referedPropName);
        }
    }


    private String getRefedUdtPropertyName(IGspCommonField field) {
        String refElementLabel = "";
        for (IGspCommonField commonField : getUdtMeta().getChildAssociations().get(0).getRefElementCollection()) {
            if (field.getRefElementId().equals(commonField.getRefElementId()))
                refElementLabel = commonField.getLabelID();
        }
        return refElementLabel;
    }

    private SimpleDataTypeDef getUdtMeta() {
        SimpleDataTypeDef udt = CMEngineUtil.getMetadataContent(field.getUdtID());
        return udt;
    }

    private String getRefedPropertyName(IGspCommonField field) {
        GspCommonModel refModel = CMEngineUtil.getMetadataContent(field.getParentAssociation().getRefModelID());
        IGspCommonElement refElement = refModel.findElementById(field.getRefElementId());
        return refElement.getLabelID();
    }
}
