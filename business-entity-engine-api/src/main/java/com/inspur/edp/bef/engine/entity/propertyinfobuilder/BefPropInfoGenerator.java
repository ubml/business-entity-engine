/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.entity.propertyinfobuilder;

import com.inspur.edp.bef.bizentity.GspBizEntityElement;
import com.inspur.edp.bef.engine.entity.propertyinfobuilder.cef.BasePropertyInfoBuilder;
import com.inspur.edp.bef.engine.entity.propertyinfobuilder.cef.CefPropInfoGenerator;
import com.inspur.edp.bef.engine.util.DataTypeConvertor;
import com.inspur.edp.bef.spi.entity.builtinimpls.BefPropertyInfo;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.DataTypePropertyInfo;
import io.iec.edp.caf.databaseobject.api.entity.DatabaseObjectTableCore;

public class BefPropInfoGenerator extends CefPropInfoGenerator {

    @Override
    public DataTypePropertyInfo getDataTypePropertyInfo(IGspCommonField field) {
        BasePropertyInfoBuilder builder = new BefPropertyInfoBuilderFactory().getBuilder(field, this);
        DataTypePropertyInfo dataTypePropertyInfo = new BefPropertyInfo(field.getLabelID(), field.getI18nResourceInfoPrefix() + ".Name",
                field.getIsRequire(),
                field.isEnableRtrim(), field.getLength(), field.getPrecision(), builder.getFieldType(field),
                builder.getObjectType(field),
                builder.getBasePropertyInfo(field), builder.getPropertyDefaultValue(field), field.getIsMultiLanguage(), ((GspBizEntityElement) field).getIsDefaultNull());
        return dataTypePropertyInfo;
    }
}
