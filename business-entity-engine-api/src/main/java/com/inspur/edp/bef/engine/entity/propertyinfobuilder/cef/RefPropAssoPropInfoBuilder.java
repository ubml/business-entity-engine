/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.entity.propertyinfobuilder.cef;

import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.engine.entity.exception.BefEngineExceptionBase;
import com.inspur.edp.bef.engine.entity.exception.ErrorCodes;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.commonmodel.engine.api.common.CMEngineUtil;
import com.inspur.edp.das.commonmodel.IGspCommonElement;

public class RefPropAssoPropInfoBuilder extends AssoPropInfoBuilder {

    public RefPropAssoPropInfoBuilder(IGspCommonField field,
                                      CefPropInfoGenerator propInfoGenerator) {
        super(field, propInfoGenerator);
    }

    @Override
    protected GspAssociation getAssociation() {
        String refModelId = field.getParentAssociation().getRefModelID();
        GspBusinessEntity refEntity = CMEngineUtil.getMetadataContent(refModelId);
        IGspCommonElement refElement = refEntity.findElementById(field.getRefElementId());
        if (refElement == null) {
            throw new BefEngineExceptionBase(ErrorCodes.BEF_ENGINE_0010, field.getBelongObject().getCode(), field.getLabelID(), field.getRefElementId());
        }
        //
        if (refElement.getChildAssociations() == null || refElement.getChildAssociations().isEmpty()) {
            throw new BefEngineExceptionBase(ErrorCodes.BEF_ENGINE_0011, field.getLabelID(), refModelId, field.getRefElementId(), refElement.getCode());
        }
        return refElement.getChildAssociations().get(0);
    }
}
