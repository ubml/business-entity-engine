/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.event.query;

import com.inspur.edp.bef.engine.entity.exception.BefEngineExceptionBase;
import com.inspur.edp.bef.engine.entity.exception.ErrorCodes;
import com.inspur.edp.cef.spi.determination.AbstractDeterminationAction;

import java.util.ArrayList;
import java.util.List;

public class BefEngineQueryEventBroker {
    private static List<IBefEngineQueryEventListener> eventLs = new ArrayList<IBefEngineQueryEventListener>() {{
        //add(getBefEngineQueryEventListener("com.inspur.edp.bef.debugtool.listener.impl.query.engine.BefEngineQueryEventListener"));
    }};

    private static IBefEngineQueryEventListener getBefEngineQueryEventListener(String className) {
        try {
            Class cls = Class.forName(className);
            IBefEngineQueryEventListener listener = (IBefEngineQueryEventListener) cls.newInstance();
            return listener;
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            throw new BefEngineExceptionBase(e,ErrorCodes.BEF_ENGINE_0009);
        }
    }

    public static final void fireProcessRecordClassPath(AbstractDeterminationAction determination) {
        for (IBefEngineQueryEventListener el : eventLs) {
            el.processRecordClassPath(determination);
        }
    }


}
