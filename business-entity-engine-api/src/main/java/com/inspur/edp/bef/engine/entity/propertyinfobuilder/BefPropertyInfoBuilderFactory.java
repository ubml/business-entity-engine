/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.entity.propertyinfobuilder;

import com.inspur.edp.bef.engine.entity.exception.BefEngineExceptionBase;
import com.inspur.edp.bef.engine.entity.exception.ErrorCodes;
import com.inspur.edp.bef.engine.entity.propertyinfobuilder.cef.AssoPropInfoBuilder;
import com.inspur.edp.bef.engine.entity.propertyinfobuilder.cef.BasePropertyInfoBuilder;
import com.inspur.edp.bef.engine.entity.propertyinfobuilder.cef.CefPropInfoGenerator;
import com.inspur.edp.bef.engine.entity.propertyinfobuilder.cef.ComplexUdtPropInfoBuilder;
import com.inspur.edp.bef.engine.entity.propertyinfobuilder.cef.DynamicPropInfoBuilder;
import com.inspur.edp.bef.engine.entity.propertyinfobuilder.cef.EnumPropInfoBuilder;
import com.inspur.edp.bef.engine.entity.propertyinfobuilder.cef.NormalPropertyInfoBuilder;
import com.inspur.edp.bef.engine.entity.propertyinfobuilder.cef.RefPropAssoPropInfoBuilder;
import com.inspur.edp.bef.engine.entity.propertyinfobuilder.cef.SimpleAssoUdtPropInfoBuilder;
import com.inspur.edp.bef.engine.entity.propertyinfobuilder.cef.SimpleEnumUdtPropInfoBuilder;
import com.inspur.edp.bef.engine.entity.propertyinfobuilder.cef.SimpleUdtPropInfoBuilder;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.commonmodel.engine.api.common.CMEngineUtil;
import com.inspur.edp.udt.designtime.api.entity.SimpleDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;

public class BefPropertyInfoBuilderFactory {

    public BefPropertyInfoBuilderFactory() {

    }

    public BasePropertyInfoBuilder getBuilder(IGspCommonField element,
                                              CefPropInfoGenerator propInfoGenerator) {
        if (element.getIsUdt() && element.getUdtID() != null && element.getUdtID() != "")
            return getUdtPropertyInfoBuilder(element, propInfoGenerator, false);
        switch (element.getObjectType()) {
            case None:
                return getNormalPropertyInfoBuilder(element, propInfoGenerator);
            case Enum:
                return getEnumPropertyInfoBuilder(element, propInfoGenerator);
            case Association:
                return getAssociationPropertyInfoBuilder(element, propInfoGenerator);
            case DynamicProp:
                return getDynamicPropPropertyInfoBuilder(element, propInfoGenerator);
            default:
                throw new BefEngineExceptionBase(ErrorCodes.BEF_ENGINE_0001);
        }
    }

    private BasePropertyInfoBuilder getDynamicPropPropertyInfoBuilder(
            IGspCommonField element,
            CefPropInfoGenerator propInfoGenerator) {
        return new DynamicPropInfoBuilder(element, propInfoGenerator);
    }

    private BasePropertyInfoBuilder getAssociationPropertyInfoBuilder(
            IGspCommonField element,
            CefPropInfoGenerator propInfoGenerator) {
        if (element.getIsRefElement() == true)
            return new RefPropAssoPropInfoBuilder(element, propInfoGenerator);
        return new AssoPropInfoBuilder(element, propInfoGenerator);
    }

    private BasePropertyInfoBuilder getEnumPropertyInfoBuilder(IGspCommonField element,
                                                               CefPropInfoGenerator propInfoGenerator) {
        return new EnumPropInfoBuilder(element, propInfoGenerator);
    }

    private BasePropertyInfoBuilder getNormalPropertyInfoBuilder(IGspCommonField element,
                                                                 CefPropInfoGenerator propInfoGenerator) {
        return new NormalPropertyInfoBuilder(element, propInfoGenerator);
    }

    private BasePropertyInfoBuilder getUdtPropertyInfoBuilder(IGspCommonField element,
                                                              CefPropInfoGenerator propInfoGenerator, boolean b) {
        UnifiedDataTypeDef unifiedDataTypeDef = CMEngineUtil.getMetadataContent(element.getUdtID());
        if (unifiedDataTypeDef == null) {
            throw new BefEngineExceptionBase(ErrorCodes.BEF_ENGINE_0007, element.getUdtID(), element.getUdtName());
        }
        if (unifiedDataTypeDef instanceof SimpleDataTypeDef) {
            SimpleDataTypeDef simpleDataTypeDef = (SimpleDataTypeDef) unifiedDataTypeDef;
            switch (simpleDataTypeDef.getObjectType()) {
                case None:
                    return new SimpleUdtPropInfoBuilder(element, propInfoGenerator, b);
                case Association:
                    return new SimpleAssoUdtPropInfoBuilder(element, propInfoGenerator, b);
                case Enum:
                    return new SimpleEnumUdtPropInfoBuilder(element, propInfoGenerator, b);
                default:
                    throw new BefEngineExceptionBase(ErrorCodes.BEF_ENGINE_0008);
            }
        } else
            return new ComplexUdtPropInfoBuilder(element, propInfoGenerator, b);
    }
}
