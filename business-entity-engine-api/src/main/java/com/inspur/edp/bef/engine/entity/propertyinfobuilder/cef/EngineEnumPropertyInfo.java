/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.entity.propertyinfobuilder.cef;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.api.manager.serialize.CefSerializeContext;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.EnumPropertyInfo;
import com.inspur.edp.cef.spi.jsonser.util.SerializerUtil;

public class EngineEnumPropertyInfo extends EnumPropertyInfo {
    public EngineEnumPropertyInfo() {
    }

    @Override
    public Object read(JsonParser reader, String propertyName, DeserializationContext serializer,
                       CefSerializeContext serContext) {
        return SerializerUtil.readString(reader);
    }

    @Override
    public Object readChange(JsonParser reader, String propertyName,
                             DeserializationContext serializer, CefSerializeContext serContext) {
        return SerializerUtil.readString(reader);
    }

    @Override
    public void write(JsonGenerator writer, String propertyName, Object value,
                      SerializerProvider serializer, CefSerializeContext serContext) {
        SerializerUtil.writeString(writer, value, propertyName, serializer);
    }

    @Override
    public void writeChange(JsonGenerator writer, String propertyName, Object value,
                            SerializerProvider serializer, CefSerializeContext serContext) {
        SerializerUtil.writeString(writer, value, propertyName, serializer);
    }
}
