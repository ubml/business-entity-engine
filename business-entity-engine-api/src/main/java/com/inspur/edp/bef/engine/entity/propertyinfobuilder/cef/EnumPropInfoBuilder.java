/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.entity.propertyinfobuilder.cef;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.ElementDataTypeConvertor;
import com.inspur.edp.cef.designtime.api.element.ElementDefaultVauleType;
import com.inspur.edp.cef.spi.entity.DefaultValueType;
import com.inspur.edp.cef.spi.entity.PropertyDefaultValue;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.BasePropertyInfo;

public class EnumPropInfoBuilder extends BasePropertyInfoBuilder {

    public EnumPropInfoBuilder(IGspCommonField field,
                               CefPropInfoGenerator propInfoGenerator) {
        super(field, propInfoGenerator);
    }

    @Override
    public BasePropertyInfo getBasePropertyInfo(IGspCommonField field) {
        return SimpleEnumUdtPropInfoBuilder.getEnumPropertyInfo(field);
    }

    @Override
    public PropertyDefaultValue getPropertyDefaultValue(IGspCommonField field) {
        if (field.getDefaultValue() == null || field.getDefaultValue() == "")
            return null;
        DefaultValueType defaultValueType = DefaultValueType.Value;
        if (field.getDefaultValueType() == ElementDefaultVauleType.Expression)
            defaultValueType = DefaultValueType.Expression;
        return new PropertyDefaultValue(field.getDefaultValue(), field.getDefaultValue(), defaultValueType);
    }
}
