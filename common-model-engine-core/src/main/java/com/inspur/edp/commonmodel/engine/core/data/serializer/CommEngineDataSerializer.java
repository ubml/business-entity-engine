/*
 *
 *  *    Copyright © OpenAtom Foundation.
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *         http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.inspur.edp.commonmodel.engine.core.data.serializer;

import com.inspur.edp.cef.spi.jsonser.entity.AbstractEntitySerializerItem;
import com.inspur.edp.commonmodel.spi.AbstractEntityDataSerializer;

import java.util.Arrays;
import java.util.List;

public class CommEngineDataSerializer extends AbstractEntityDataSerializer {

    protected CommEngineDataSerializer() {
        super(null, false, getTypes());
    }

    private static List<AbstractEntitySerializerItem> items = Arrays
            .asList(new CommonEngineDataSerializerItem());

    private static List<AbstractEntitySerializerItem> getTypes() {
        return  Arrays
                .asList(new CommonEngineDataSerializerItem());
    }

    @Override
    protected AbstractEntityDataSerializer getChildConvertor(String childCode) {
        return new CommEngineDataSerializer();
    }
}
