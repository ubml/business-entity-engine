/*
 *
 *  *    Copyright © OpenAtom Foundation.
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *         http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.inspur.edp.commonmodel.engine.core.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.core.data.EntityDataBase;
import com.inspur.edp.cef.core.data.EntityDataCollection;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.element.GspElementObjectType;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IChildEntityData;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.entity.entity.IEntityDataCollection;
import com.inspur.edp.cef.entity.entity.IMultiLanguageData;
import com.inspur.edp.cef.entity.entity.IValuesContainer;
import com.inspur.edp.cef.entity.entity.dynamicProp.DynamicPropSetImpl;
import com.inspur.edp.cef.entity.i18n.MultiLanguageInfo;
import com.inspur.edp.cef.spi.entity.resourceInfo.EntityResInfo;
import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefEntityResInfoImpl;
import com.inspur.edp.commonmodel.engine.api.data.AssociationInfo;
import com.inspur.edp.commonmodel.engine.api.data.IEngineEntityData;
import com.inspur.edp.commonmodel.engine.core.common.CMUtil;
import com.inspur.edp.commonmodel.engine.core.data.serializer.CommEngineDataSerializer;
import com.inspur.edp.commonmodel.engine.core.exception.CMEngineCoreException;
import com.inspur.edp.commonmodel.engine.core.exception.ErrorCodes;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.IObjectCollection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

@JsonSerialize(using = CommEngineDataSerializer.class)
public class EngineChildData extends EntityDataBase
        implements Cloneable, IChildEntityData, IValuesContainer, IMultiLanguageData, IEngineEntityData {

    @JSONField(serialize = false)
    protected final IGspCommonObject node;
    protected final CefEntityResInfoImpl resInfo;
    protected HashMap<String, Object> values;
    protected HashMap<String, IEntityDataCollection> childs;

    @Deprecated
    public EngineChildData(IGspCommonObject node) {
        this(node, null);
    }

    public EngineChildData(IGspCommonObject node, CefEntityResInfoImpl resInfo) {
        this.node = node;
        this.resInfo = resInfo;
    }

    protected HashMap<String, Object> getValues() {
        if (values == null) {
            values = new HashMap<>(node.getContainElements().size(), 1);
        }
        return values;
    }

    private String parentLabelId;

    private String getParentLabelId() {
        if (parentLabelId == null) {
            parentLabelId = node.findElement(node.getKeys().get(0).getSourceElement()).getLabelID();
        }
        return parentLabelId;
    }

    @Override
    public String getParentID() {
        Object value = getValue(getParentLabelId());
        return CMUtil.convert2String(value);
    }

    @Override
    public void setParentID(String s) {
        setValue(getParentLabelId(), s);
    }

    @Override
    public HashMap<String, IEntityDataCollection> innerGetChilds() {
        return (HashMap<String, IEntityDataCollection>) getInnerGetChilds().clone();
    }

    protected HashMap<String, IEntityDataCollection> getInnerGetChilds() {
        if (childs == null) {
            IObjectCollection childNodes = node.getContainChildObjects();
            childs = new HashMap<>(childNodes.size(), 1);
            for (IGspCommonObject child : childNodes) {
                CefEntityResInfoImpl childResInfo = getChildResInfo(child);
                EntityDataCollection collection = childResInfo == null ? new EntityDataCollection() : new EntityDataCollection(childResInfo);
                childs.put(child.getCode(), collection);
            }
        }
        return childs;
    }

    @Override
    public ICefData innerCreateChild(String s) {
        IGspCommonObject childNode = CMUtil.checkNodeExists(node, s, null);
        EntityResInfo childResInfo = getChildResInfo(childNode);
        return new EngineChildData(childNode, (CefEntityResInfoImpl) childResInfo);
    }

    private CefEntityResInfoImpl getChildResInfo(IGspCommonObject childNode) {
        return resInfo != null ? (CefEntityResInfoImpl) resInfo
                .getChildEntityResInfo(childNode.getCode()) : null;
    }

    @Override
    public void innerMergeChildData(String s, ArrayList<IEntityData> childData) {
        IEntityDataCollection childDataCollection = getChilds().get(s);
        if (childDataCollection == null) {
            CMUtil.throwNodeNotFound(node, s);
        }
        for (IEntityData data : childData) {
            childDataCollection.add(data);
        }
    }

    @Override
    public Object innerGetValue(String labeldId) {
        IGspCommonElement element = CMUtil.checkElementExists(node, labeldId, null);
        if (element.getObjectType() == GspElementObjectType.DynamicProp) {
            Object rez = getValues().get(labeldId);
            if (rez == null) {
                getValues().put(labeldId, (rez = new DynamicPropSetImpl()));
            }
            return rez;
        } else if (!element.getIsRefElement()) {
            return getValues().get(labeldId);
        } else {
            String parentLabelId = element.getParentAssociation().getBelongElement().getLabelID();
            Object parentValue = getValues().get(parentLabelId);
            if (parentValue == null) {
                return null;
            } else {
                return ((AssociationInfo) parentValue).getValue(labeldId);
            }
        }
    }


    @Override
    public com.inspur.edp.cef.entity.entity.ICefData innerCopySelf() {
        try {
            EngineChildData result = (EngineChildData) this.clone();
            result.values = CMUtil.cloneForValues(this.values);
            return result;
        } catch (CloneNotSupportedException e) {
            throw new CMEngineCoreException(e);
        }
    }

    @Override
    public ICefData innerCopy() {
        try {
            EngineChildData result = (EngineChildData) this.clone();
            result.values = CMUtil.cloneForValues(this.values);
            if (childs != null) {
                result.childs = new HashMap<>(childs.size(), 1);
                for (Map.Entry<String, IEntityDataCollection> pair : childs.entrySet()) {
                    EntityDataCollection dataCollection = new EntityDataCollection();
                    for (IEntityData item : pair.getValue()) {
                        dataCollection.add((IEntityData) item.copy());
                    }
                    result.childs.put(pair.getKey(), dataCollection);
                }
            }
            return result;
        } catch (CloneNotSupportedException e) {
            throw new CMEngineCoreException();
        }
    }

    @Override
    public List<String> innerGetPropertyNames() {
        return CMUtil.buildNodePropertyNames(node, getElementPredicate());
    }

    @Override
    public void innerSetValue(String s, Object o) {
        CMUtil.checkElementValue(node, s, o, getElementPredicate());
        getValues().put(s, o);
    }

    @Override
    public Object innerCreateValue(String propertyName) {
        GspAssociation gspAssociation = null;
        for (IGspCommonField gspCommonField : node.getContainElements()) {
            if (gspCommonField.getLabelID().equals(propertyName) && gspCommonField.getObjectType() == GspElementObjectType.Association) {
                gspAssociation = gspCommonField.getChildAssociations().get(0);
            }
        }
        return new AssociationInfo(gspAssociation);
    }

    @Override
    public String getID() {
        Object rez = getValues().get(node.getIDElement().getLabelID());
        if (rez == null)
            return null;
        else if (rez instanceof String)
            return (String) rez;
        else {
            throw new CMEngineCoreException(ErrorCodes.CM_ENGINE_1019,
                    node.getBelongModel().getID(), node.getBelongModel().getCode(),
                    node.getBelongModel().getName(), node.getCode(),
                    String.valueOf(node.getIDElement().getObjectType()), rez.getClass().getName());
        }
    }

    @Override
    public void setID(String s) {
        getValues().put(node.getIDElement().getLabelID(), s);
    }

    //#region virtual
    protected Predicate<IGspCommonField> getElementPredicate() {
        return null;
    }

    protected Predicate<IGspCommonObject> getNodePredicate() {
        return null;
    }

    private Map<String, MultiLanguageInfo> multiLanguageInfos;

    @JsonIgnore
    @Deprecated
    public Map<String, MultiLanguageInfo> getMultiLanguageInfos() {
        if (this.multiLanguageInfos == null) {
            this.multiLanguageInfos = new HashMap<>();
        }
        return multiLanguageInfos;
    }

    @Override
    @JSONField(serialize = false)
    public final IGspCommonObject getNode() {
        return node;
    }
    //#endregion
}
