package com.inspur.edp.commonmodel.engine.core.exception;

public class ErrorCodes {
    /**
     * ID为自动生成, 不可修改
     */
    public static String CM_ENGINE_1001 = "CM_ENGINE_1001";
    /**
     * 当前MDataType无法被识别{0}.{1}
     * 0:(IGspCommonField)element.getLabelID() 1:(IGspCommonField)element.getMDataType()
     */
    public static String CM_ENGINE_1002 = "CM_ENGINE_1002";
    /**
     * 获取Udt元数据失败{0}
     * 元数据id
     */
    public static String CM_ENGINE_1003 = "CM_ENGINE_1003";
    /**
     * Udt元数据不存在{0}
     * 元数据id
     */
    public static String CM_ENGINE_1004 = "CM_ENGINE_1004";
    /**
     * 关联类型数据序列化失败
     */
    public static String CM_ENGINE_1005 = "CM_ENGINE_1005";
    /**
     * value{0}对{1}.{2}无效
     */
    public static String CM_ENGINE_1006 = "CM_ENGINE_1006";
    /**
     * 不支持将默认值{0}设置在关联字段{1}上
     */
    public static String CM_ENGINE_1007 = "CM_ENGINE_1007";
    /**
     * ID编号名称为{0}，{1}，{2}的模型的节点{3}的字段{4}上设置的默认值{5}格式不正确, 请联系元数据开发人员修改
     * 0:模型ID 1:模型编号 2:模型名称 3:节点 4:字段 5:字段设置默认值
     */
    public static String CM_ENGINE_1008 = "CM_ENGINE_1008";
    /**
     * 不支持将默认值{0}设置在二进制字段{1}上
     */
    public static String CM_ENGINE_1009 = "CM_ENGINE_1009";
    /**
     * 元数据字段{0}{1}的数据类型{2}错误
     * 0:元数据字段id 1:元数据字段名称 2：数据类型
     */
    public static String CM_ENGINE_1010 = "CM_ENGINE_1010";
    /**
     * 时间日期类型数据格式转换出错
     */
    public static String CM_ENGINE_1011 = "CM_ENGINE_1011";
    /**
     * 找不到节点{0}.{1}
     * (IGspCommonObject.getCode()).childCode
     */
    public static String CM_ENGINE_1012 = "CM_ENGINE_1012";
    /**
     * 找不到字段{0}.{1}
     * (IGspCommonObject.getCode()).labelId
     */
    public static String CM_ENGINE_1013 = "CM_ENGINE_1013";
    /**
     * 找不到自定义动作{0}.{1}
     * (IGspCommonModel.getCode()).actionCode
     */
    public static String CM_ENGINE_1014 = "CM_ENGINE_1014";
    /**
     * 关联带出字段不存在{0}.{1}
     * (GspAssociation.getBelongElement().getLabelID()).labelId
     */
    public static String CM_ENGINE_1015 = "CM_ENGINE_1015";
    /**
     * 字段{0}为业务字段或动态属性字段, 不支持直接赋值, 如通过变更集赋值,请参考此文档正确构造变更集:https://open.inspuronline.com/iGIX/#/document/mddoc/docs-gsp-cloud-ds%2Fdev-guide-beta%2Fspecial-subject%2FBE-related%2F%E5%8F%98%E6%9B%B4%E9%9B%86%E4%BD%BF%E7%94%A8.md
     */
    public static String CM_ENGINE_1016 = "CM_ENGINE_1016";
    /**
     * 暂不支持字段类型{0}
     */
    public static String CM_ENGINE_1017 = "CM_ENGINE_1017";
    /**
     * 关联带出字段不存在{0}.{1}
     * (IGspCommonField.getLabelID()).(JsonParser.getValueAsString())
     */
    public static String CM_ENGINE_1018 = "CM_ENGINE_1018";
    /**
     * 模型{0}{1}{2}上编号为{3}的节点的主键字段类型不是普通字符串, 元数据字段类型为{4}, 实际值类型为{5}, 请修改Idp表单后重新推送
     * 0:ID 1:编号 2:名称 3:节点编号 4:元数据字段类型 5:元数据实际类型
     */
    public static String CM_ENGINE_1019 = "CM_ENGINE_1019";
    /**
     * 关联必须以ObjectToken开始
     */
    public static String CM_ENGINE_1020 = "CM_ENGINE_1020";
    /**
     * 暂不支持动态属性类型的变更集，当前字段标签{0}
     */
    public static String CM_ENGINE_1021 = "CM_ENGINE_1021";
    /**
     * 当前类型为{0}的字段暂不支持进行变更数据，当前字段标签为{1}
     */
    public static String CM_ENGINE_1022 = "CM_ENGINE_1022";
    /**
     * 当前字段类型不支持被序列化{0}.{1}
     * 0:(IGspCommonField)element.getLabelID() 1:(IGspCommonField)element.getMDataType()
     */
    public static String CM_ENGINE_1023 = "CM_ENGINE_1023";
    /**
     * 当前data类型[{0}]不支持序列化
     */
    public static String CM_ENGINE_1024 = "CM_ENGINE_1024";

}
