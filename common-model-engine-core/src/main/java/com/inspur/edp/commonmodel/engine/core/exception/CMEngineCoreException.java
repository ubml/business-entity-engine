package com.inspur.edp.commonmodel.engine.core.exception;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

public class CMEngineCoreException extends CAFRuntimeException {
    private static final String SU = "pfcommon";

    private static final String RESOURCE_FILE = "cm_engine_core_exception.properties";

    public CMEngineCoreException(Exception exception, String errorCodes, String... messageParams) {
        super(SU, RESOURCE_FILE, errorCodes, messageParams, exception, ExceptionLevel.Error, true);
    }

    public CMEngineCoreException(String errorcodes, String... messageParams) {
        super(SU, RESOURCE_FILE, errorcodes, messageParams, null, ExceptionLevel.Error, true);
    }

    public CMEngineCoreException(Exception e) {
        super(SU, null, null, e);
    }

    public CMEngineCoreException() {
        super(SU, null, null, null);
    }
}
