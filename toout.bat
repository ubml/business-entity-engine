@echo off

for /f "tokens=*" %%i in ('CALL .\xpath0.1.bat pom.xml "/project/version"') do set version=%%i
ECHO version=%version%

DEL /S/Q .\out

MKDIR .\out\server\platform\common\libs
MKDIR .\out\server\platform\common\resources

COPY .\business-entity-engine-api\target\business-entity-engine-api-%version%.jar .\out\server\platform\common\libs\bef-engine-api.jar
COPY .\business-entity-engine-core\target\business-entity-engine-core-%version%.jar .\out\server\platform\common\libs\bef-engine-core.jar
COPY .\business-entity-engine-repository\target\business-entity-engine-repository-%version%.jar .\out\server\platform\common\libs\bef-engine-repository.jar
COPY .\common-model-engine-core\target\common-model-engine-core-%version%.jar .\out\server\platform\common\libs\cm-engine-core.jar

::XCOPY .\i18n\bef_engine_repository.resources  .\out\server\platform\common\resources /S
::XCOPY .\i18n\server\platform\runtime\trace\resources  .\out\server\platform\runtime\trace\resources /S
::pause