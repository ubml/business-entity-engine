/*
 *
 *  *    Copyright © OpenAtom Foundation.
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *         http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.inspur.edp.cef.variableengine.core.manager;

import com.inspur.edp.cef.designtime.api.variable.CommonVariableEntity;
import com.inspur.edp.cef.variableengine.core.data.VariableData;
import com.inspur.edp.cef.variableengine.core.data.serializer.EngineVarChangeDeserializer;
import com.inspur.edp.cef.variableengine.core.data.serializer.EngineVarChangeSerializer;
import com.inspur.edp.cef.variableengine.core.entity.EngineVariableEntity;
import com.inspur.edp.cef.variable.api.variable.IVariableContext;
import com.inspur.edp.cef.variable.core.manager.AbstractVariableManager;
import com.inspur.edp.cef.variable.core.variable.AbstractVariable;
import java.util.Objects;

public class EngineVariableManager extends AbstractVariableManager {

  private final CommonVariableEntity node;

  public EngineVariableManager(CommonVariableEntity node) {
    Objects.requireNonNull(node, "node");

    this.node = node;
  }

  protected CommonVariableEntity getCommonVariable(){
    return node;
  }

  @Override
  public com.inspur.edp.cef.api.dataType.base.IAccessorCreator getAccessorCreator() {
    return new EngineVariableAccessorCreator(node);
  }

  @Override
  protected com.inspur.edp.cef.spi.jsonser.abstractcefchange.AbstractCefChangeSerializer getChangeSerializer() {
    return new EngineVarChangeSerializer(node);
  }

  @Override
  protected com.inspur.edp.cef.spi.jsonser.abstractcefchange.AbstractCefChangeJsonDeserializer getChangeDeserializer() {
    return new EngineVarChangeDeserializer(node);
  }

  @Override
  protected com.inspur.edp.cef.entity.entity.IValueObjData createDataCore() {
    return new VariableData(node);
  }

  @Override
  public AbstractVariable createVariable(IVariableContext ctx) {
    return new EngineVariableEntity(node, ctx);
  }
}
