/*
 *
 *  *    Copyright © OpenAtom Foundation.
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *         http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.inspur.edp.cef.variableengine.core.entity;

import com.inspur.edp.cef.api.dataType.valueObj.ICefValueObjContext;
import com.inspur.edp.cef.designtime.api.variable.CommonVariableEntity;
import com.inspur.edp.cef.variableengine.core.dtm.VariableEngineDtmAdaptor;
import com.inspur.edp.cef.variableengine.core.dtm.VariableEngineTransNestedDtmAdaptor;
import com.inspur.edp.cef.spi.determination.IDetermination;
import com.inspur.edp.cef.spi.determination.IValueObjRTDtmAssembler;
import com.inspur.edp.cef.variable.core.determination.VarDeterminationContext;
import java.util.List;
import java.util.stream.Collectors;

public class EngineVarAfterModifyDtmAssembler implements IValueObjRTDtmAssembler {

  private final CommonVariableEntity varEntity;

  public EngineVarAfterModifyDtmAssembler(CommonVariableEntity varEntity) {
    this.varEntity = varEntity;
  }

  private List<IDetermination> determinationsList;

  private java.util.List<IDetermination> belongingDeterminationsList;

  @Override
  public List<IDetermination> getDeterminations() {
    if (determinationsList == null) {
      determinationsList = varEntity.getDtmAfterModify().stream()
          .map(item -> new VariableEngineDtmAdaptor(varEntity, item)).collect(Collectors.toList());
    }
    return determinationsList;
  }

  @Override
  public java.util.List<IDetermination> getBelongingDeterminations() {
    if (belongingDeterminationsList == null) {
      belongingDeterminationsList = varEntity.getContainElements().stream().filter(item -> item.getIsUdt())
          .map(item -> new VariableEngineTransNestedDtmAdaptor(item)).collect(Collectors.toList());
    }
    return belongingDeterminationsList;
  }

  @Override
  public java.util.List<IDetermination> getChildAssemblers() {
    return null;
  }

  @Override
  public com.inspur.edp.cef.entity.changeset.IChangeDetail getChangeset(
      com.inspur.edp.cef.api.dataType.base.ICefDataTypeContext dataTypeContext) {
    throw new java.lang.UnsupportedOperationException();
  }

  @Override
  public com.inspur.edp.cef.api.determination.ICefDeterminationContext getDeterminationContext(
      ICefValueObjContext data) {
    return new VarDeterminationContext(data);
  }
}
