/*
 *
 *  *    Copyright © OpenAtom Foundation.
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *         http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.inspur.edp.cef.variableengine.core.manager;

import com.inspur.edp.cef.api.dataType.base.IAccessorCreator;
import com.inspur.edp.cef.designtime.api.variable.CommonVariableEntity;
import com.inspur.edp.cef.variableengine.core.data.VariableAccessor;
import com.inspur.edp.cef.variableengine.core.data.VariableReadonlyAccessor;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IValueObjData;
import java.util.Objects;

public class EngineVariableAccessorCreator implements IAccessorCreator {

  private final CommonVariableEntity node;

  public EngineVariableAccessorCreator(CommonVariableEntity node) {
    Objects.requireNonNull(node, "node");

    this.node = node;
  }

  @Override
  public com.inspur.edp.cef.entity.accessor.base.IAccessor createAccessor(ICefData inner) {
    return new VariableAccessor(node, (IValueObjData) inner);
  }

  @Override
  public com.inspur.edp.cef.entity.accessor.base.IAccessor createReadonlyAccessor(ICefData inner) {
    return new VariableReadonlyAccessor(node, (IValueObjData) inner);
  }

}
