/*
 *
 *  *    Copyright © OpenAtom Foundation.
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *         http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.inspur.edp.cef.variableengine.core.data.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.variable.CommonVariableEntity;
import com.inspur.edp.cef.engine.core.data.serializer.EngineSerializerItem;
import com.inspur.edp.cef.entity.entity.IValueObjData;
import com.inspur.edp.cef.spi.jsonser.valueobj.AbstractValueObjSerializer;
import java.math.BigDecimal;
import java.util.Date;

//默认的序列化器, 如果没有的话
public class CommonVarSerializerItem extends AbstractValueObjSerializer {

  @Override
  public void writeEntityBasicInfo(JsonGenerator writer, IValueObjData data,
      SerializerProvider serializer) {
    for (String name : data.getPropertyNames()) {
      Object value = data.getValue(name);
      if (value instanceof Date) {
        this.writeDateTime(writer, value, name, serializer);
      } else if(value instanceof String){
        this.writeString(writer, value, name, serializer);
      }else if(value instanceof Integer){
        this.writeInt(writer, value, name, serializer);
      }else if(value instanceof Boolean){
        this.writeBool(writer, value, name, serializer);
      }else if(value instanceof BigDecimal){
        this.writeDecimal(writer, value, name, serializer);
      }else if(value instanceof Byte[]){
        this.writeBytes(writer, value, name, serializer);
      }else{
        this.writeBaseType(writer, value, name, serializer);
      }
    }
  }

  @Override
  public boolean readEntityBasicInfo(JsonParser jsonParser,
      DeserializationContext deserializationContext, IValueObjData iValueObjData, String s) {
    return false;
  }

  @Override
  public boolean writeModifyPropertyJson(JsonGenerator jsonGenerator, String s, Object o,
      SerializerProvider serializerProvider) {
    return false;
  }
}
