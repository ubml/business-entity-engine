/*
 *
 *  *    Copyright © OpenAtom Foundation.
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *         http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.inspur.edp.cef.variableengine.core.data;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.designtime.api.variable.CommonVariableEntity;
import com.inspur.edp.cef.engine.core.data.EngineValueObjAccessor;
import com.inspur.edp.cef.entity.entity.IValueObjData;
import com.inspur.edp.cef.variable.api.data.IVariableData;
import com.inspur.edp.cef.variableengine.core.data.serializer.CommVarEngineDataSerializer;

@JsonSerialize(using = CommVarEngineDataSerializer.class)
public class VariableAccessor extends EngineValueObjAccessor implements IVariableData {

  public VariableAccessor(CommonVariableEntity node, IValueObjData inner) {
    super(node, inner);
  }

  @Override
  protected CommonVariableEntity getDataType() {
    return (CommonVariableEntity) super.getDataType();
  }

//  public static void main(String[] args) throws JsonProcessingException {
//    CommonVariableEntity node = new CommonVariableEntity();
//    VariableAccessor acc = new VariableAccessor(node, new VariableData(node));
//    System.out.println(new ObjectMapper().writeValueAsString(acc));
//  }
}
