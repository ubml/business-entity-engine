package com.inspur.edp.cef.variableengine.core.exception;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

public class VariableEngineCoreException extends CAFRuntimeException {
    private static final String SU = "pfcommon";

    public VariableEngineCoreException(Exception e) {
        super(SU, null, null, e);
    }

}

