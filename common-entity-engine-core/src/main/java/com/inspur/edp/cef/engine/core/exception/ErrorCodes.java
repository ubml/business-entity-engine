package com.inspur.edp.cef.engine.core.exception;

public class ErrorCodes {
    /**
     * {0}对于{1}.{2}无效
     */
    public static String CEF_ENGINE_0001 = "CEF_ENGINE_0001";
    /**
     * udt元数据不存在{0}
     * 0:udt.ID
     */
    public static String CEF_ENGINE_0002 = "CEF_ENGINE_0002";
    /**
     * 找不到字段{0}.{1}
     */
    public static String CEF_ENGINE_0003 = "CEF_ENGINE_0003";
    /**
     * 不支持序列化此字段类型{0}.{1}
     */
    public static String CEF_ENGINE_0004 = "CEF_ENGINE_0004";
    /**
     * 无法识别的MDataType{0}.{1}
     */
    public static String CEF_ENGINE_0005 = "CEF_ENGINE_0005";
    /**
     * 暂不支持的字段类型{0}
     */
    public static String CEF_ENGINE_0006 = "CEF_ENGINE_0006";
    /**
     * 暂不支持关联字段
     */
    public static String CEF_ENGINE_0007 = "CEF_ENGINE_0007";
}