package com.inspur.edp.cef.engine.core.exception;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

public class CefEngineCoreException extends CAFRuntimeException {

    private static final String SU = "pfcommon";

    private static final String RESOURCE_FILE = "cef_engine_core_exception.properties";

    public CefEngineCoreException(String errorcodes, String... messageParams) {
        super(SU, RESOURCE_FILE, errorcodes, messageParams, null, ExceptionLevel.Error, true);
    }

    public CefEngineCoreException(Exception e) {
        super(SU, null, null, e);
    }

    public CefEngineCoreException() {
        super(SU, null, null, null);
    }
}