/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.core.dtm;

import com.inspur.edp.bef.api.action.determination.IDeterminationContext;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.beenum.BETriggerTimePointType;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.determination.IDetermination;
import com.inspur.edp.common.component.actionextension.AttachmentBeforSaveDetermination;
import com.inspur.edp.common.component.actionextension.CancelAttachmentTempDeter;
import com.inspur.edp.common.component.actionextension.DelPathTreeCheck;
import com.inspur.edp.common.component.actionextension.DeleteDetermination;
import com.inspur.edp.common.component.actionextension.ParentTreeDeleteDeter;
import com.inspur.edp.common.component.actionextension.TreeBeforSaveDetermination;
import com.inspur.edp.common.component.timestamp.CreatedByBeforeSaveDetermination;
import com.inspur.edp.common.component.timestamp.LastModifiedByBeforeSaveDetermination;
import com.inspur.edp.common.component.timestamp.TimeStampDetermination;
import lombok.extern.slf4j.Slf4j;

/**
 * 内置udt构件适配
 */
@Slf4j
public class BuiltInEngineDtmAdaptor implements IDetermination {
    private IGspCommonField gspCommonField;
    private GspBizEntityObject gspBizEntityObject;
    private Object String;
    private BETriggerTimePointType triggerType;//触发时机

    public BuiltInEngineDtmAdaptor(GspBizEntityObject gspBizEntityObject, IGspCommonField gspCommonField, BETriggerTimePointType triggerType) {
        this.gspBizEntityObject = gspBizEntityObject;
        this.gspCommonField = gspCommonField;
        this.triggerType = triggerType;
    }

    @Override
    public String getName() {
        return gspCommonField.getName();
    }

    @Override
    public boolean canExecute(IChangeDetail change) {
        if (gspCommonField.getUdtID().equals("21a5a440-523f-4558-a432-7d71b5e19dfe")) {//时间戳
            return change.getChangeType() != ChangeType.Deleted;
        }
        if (gspCommonField.getUdtID().equals("dbfbe55d-ba65-4a7f-a9d4-4f664ec6ec68")) {//分级码
            return change.getChangeType() == ChangeType.Deleted || change.getChangeType() == ChangeType.Added;
        }
        if (gspCommonField.getUdtID().equals("12be876c-368c-4262-88ab-4112688540b0")) {//父节点
            return change.getChangeType() == ChangeType.Deleted;
        }
        if (gspCommonField.getUdtID().equals("ee86e96e-6d51-48c7-9e92-9b7dfe3b43b6")) {//附件
            return true;
        }
        if (gspCommonField.getUdtID().equals("91025792-e305-4662-8e80-47cd4980d06f")) {//创建人
            return true;
        }
        if (gspCommonField.getUdtID().equals("f5bf18b7-0a9d-4365-8ed8-655d1e1aa70f")) {//最后修改人
            return true;
        }
        return true;
    }

    @Override
    public void execute(ICefDeterminationContext context, IChangeDetail change) {
        if (gspCommonField.getUdtID().equals("21a5a440-523f-4558-a432-7d71b5e19dfe")) {//时间戳
            new TimeStampDetermination((IDeterminationContext) context, change, gspCommonField.getLabelID()).execute();
        }
        if (gspCommonField.getUdtID().equals("dbfbe55d-ba65-4a7f-a9d4-4f664ec6ec68") && gspBizEntityObject.getBelongModel().getEnableTreeDtm()) {//分级码
            //分级码树处理逻辑：
            //新增行：通过公共构件新增行，如：分级码分级方式新增同级、分级码分级方式新增子级等。在构件中除为新增行赋默认分级码外，还更新了新增行的父节点中分级码的是否明细属性。
            //新增行-保存前：根据分级码缓存重新生成分级码，并更新分级码缓存。
            //修改行：使用BEF的通用接口直接修改数据。分级码相关逻辑完全不参与
            //删除行：使用BEF的通用接口直接删除数据。
            //删除行-修改后：删除行时，需要判断删除的节点是否有同级节点，如果没有则更新父节点的是否明细值为否。不能在保存前处理该逻辑，否则删除的节点如果是刚刚新增的还没有保存，不触发保存前事件，会造成父节点的是否明细还是否。
            if (triggerType == BETriggerTimePointType.AfterModify) {
                if (change.getChangeType() == ChangeType.Deleted) {
                    new DeleteDetermination((IDeterminationContext) context, change, gspCommonField.getLabelID()).execute();
                    new DelPathTreeCheck((IDeterminationContext) context, change, gspCommonField.getLabelID()).execute();
                }
            } else if (triggerType == BETriggerTimePointType.BeforeCheck) {
                if (change.getChangeType() == ChangeType.Added) {
                    new TreeBeforSaveDetermination((IDeterminationContext) context, change, gspCommonField.getLabelID()).execute();
                }
            } else {
                log.warn("Should not execute path tree builtin engine dtm when trigger type is: {}", triggerType);
            }
        }
        if (gspCommonField.getUdtID().equals("12be876c-368c-4262-88ab-4112688540b0")) {//父节点
            new ParentTreeDeleteDeter((IDeterminationContext) context, change, gspCommonField.getLabelID()).execute();
        }
        if (gspCommonField.getUdtID().equals("ee86e96e-6d51-48c7-9e92-9b7dfe3b43b6")) {//附件
            if (triggerType == BETriggerTimePointType.BeforeCheck) {//保存前
                new AttachmentBeforSaveDetermination((IDeterminationContext) context, change, gspCommonField.getLabelID()).execute();
            } else if (triggerType == BETriggerTimePointType.Cancel) {//取消
                new CancelAttachmentTempDeter((IDeterminationContext) context, change, gspCommonField.getLabelID()).execute();
            }
        }
        if ("91025792-e305-4662-8e80-47cd4980d06f".equals(gspCommonField.getUdtID())) {
            new CreatedByBeforeSaveDetermination((IDeterminationContext) context, change, gspCommonField.getLabelID()).execute();
        }
        if ("f5bf18b7-0a9d-4365-8ed8-655d1e1aa70f".equals(gspCommonField.getUdtID())) {
            new LastModifiedByBeforeSaveDetermination((IDeterminationContext) context, change, gspCommonField.getLabelID()).execute();
        }
    }
}
