/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.entity;

import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefEntityResInfoImpl;
import com.inspur.edp.das.commonmodel.IGspCommonObject;

@Deprecated
public class EngineRootReadonlyAccessor extends
        com.inspur.edp.commonmodel.engine.core.data.EngineRootReadonlyAccessor {

    public EngineRootReadonlyAccessor(IGspCommonObject node, IEntityData data,
                                      CefEntityResInfoImpl resInfo) {
        super(node, data, resInfo);
    }
}
