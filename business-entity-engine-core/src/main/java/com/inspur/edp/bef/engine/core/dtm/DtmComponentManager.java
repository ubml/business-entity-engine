/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.core.dtm;

import com.inspur.edp.bef.engine.core.dtm.builtin.AttachOnCancelDtmAdaptor;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.spi.determination.IDetermination;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class DtmComponentManager {

  private static List<String> beforeSaveList = new ArrayList<>();
  private static List<String> afterModifyList = new ArrayList<>();
  private static List<String> afterSaveValList = new ArrayList<>();

  //获取保存前内置构件
  public static List<String> getBeforeSaveComponents() {
    if (beforeSaveList.size() == 0) {
      beforeSaveList = new ArrayList<>();
      beforeSaveList.add("ee86e96e-6d51-48c7-9e92-9b7dfe3b43b6");//Attachment附件
      beforeSaveList.add("21a5a440-523f-4558-a432-7d71b5e19dfe");//TimeStamp时间戳
      beforeSaveList.add("dbfbe55d-ba65-4a7f-a9d4-4f664ec6ec68");//树-分级码分级信息
      beforeSaveList.add("12be876c-368c-4262-88ab-4112688540b0");//树-父节点分级信息
      beforeSaveList.add("91025792-e305-4662-8e80-47cd4980d06f");
      beforeSaveList.add("f5bf18b7-0a9d-4365-8ed8-655d1e1aa70f");
//            beforeSaveList.add("bd050131-e9ed-47e8-b08f-533b28928c5f");//SecurityLevel 密集
//            beforeSaveList.add("a0b19650-0101-468f-ae3f-40c76c0f06b0");//提交审批-状态
    }
    return beforeSaveList;
  }
  //获取保存后验证内置构件
  public static List<String> getAfterSaveValComponents() {
    if (afterSaveValList.size() == 0) {
      afterSaveValList = new ArrayList<>();
      afterSaveValList.add("ee86e96e-6d51-48c7-9e92-9b7dfe3b43b6");//Attachment附件
//            beforeSaveList.add("bd050131-e9ed-47e8-b08f-533b28928c5f");//SecurityLevel 密集
//            beforeSaveList.add("a0b19650-0101-468f-ae3f-40c76c0f06b0");//提交审批-状态
    }
    return afterSaveValList;
  }

  public static List<String> getAfterModifyComponents() {
    if(afterModifyList.size() == 0){
      afterModifyList = new ArrayList<>();
      afterModifyList.add("dbfbe55d-ba65-4a7f-a9d4-4f664ec6ec68");//分级码
    }
    return afterModifyList;
  }

  //region onCancel
  private volatile static Map<String, Function<IGspCommonField, IDetermination>> onCancelList;

  private static Object onCancelLock = new Object();

  public static Map<String, Function<IGspCommonField, IDetermination>> getOnCancelBuiltinComponents() {
    if (onCancelList == null) {
      synchronized (onCancelLock) {
        if (onCancelList == null) {
          onCancelList = new HashMap<>(1);
          onCancelList.put("ee86e96e-6d51-48c7-9e92-9b7dfe3b43b6",
              (field) -> new AttachOnCancelDtmAdaptor() {{
                setField(field);
              }});//Attachment附件
        }
      }
    }
    return onCancelList;
  }
  //endregion
}
