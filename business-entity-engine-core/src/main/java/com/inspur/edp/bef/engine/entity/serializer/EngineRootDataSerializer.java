/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.entity.serializer;

import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.engine.common.GspBizEntityUtil;
import com.inspur.edp.bef.spi.entity.AbstractBizEntitySerializer;
import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefEntityResInfoImpl;
import com.inspur.edp.cef.spi.jsonser.entity.AbstractEntitySerializerItem;
import com.inspur.edp.das.commonmodel.IGspCommonModel;
import com.inspur.edp.das.commonmodel.IGspCommonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EngineRootDataSerializer extends AbstractBizEntitySerializer {

    private IGspCommonModel be;
    private final CefEntityResInfoImpl resInfo;

    public EngineRootDataSerializer(IGspCommonModel be, CefEntityResInfoImpl resInfo) {
        super(be.getMainObject().getCode(), true, be.getGeneratedConfigID(), getTypes(resInfo));
        this.be = be;
        this.resInfo = resInfo;
    }

    private static List<AbstractEntitySerializerItem> getTypes(CefEntityResInfoImpl resInfo) {
        java.util.ArrayList<AbstractEntitySerializerItem> list = new ArrayList<>();
        list.add(new com.inspur.edp.cef.spi.jsonser.builtinimpls.CefEntityDataSerializerItem(resInfo));
        return list;
    }

    @Override
    protected AbstractBizEntitySerializer getChildConvertor(String childCode) {
        IGspCommonObject childNode = GspBizEntityUtil
                .checkNodeExists(be.getMainObject(), childCode, null);
        return new EngineChildDataSerializer(childNode,
                (CefEntityResInfoImpl) resInfo.getChildEntityResInfo(childNode.getCode()));
    }
}
