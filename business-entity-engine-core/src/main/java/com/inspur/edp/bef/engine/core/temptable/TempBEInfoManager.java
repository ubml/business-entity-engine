/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.core.temptable;

import java.util.HashMap;

//todo 是否加个自动过期删除
public class TempBEInfoManager {
    private HashMap<String, String> hash = new HashMap<>();
    private static TempBEInfoManager instance = new TempBEInfoManager();

    private TempBEInfoManager() {
    }

    public static TempBEInfoManager getInstance() {
        return instance;
    }

    public void addItem(String configId, String beId) {
        hash.put(configId, beId);
    }

    public String getItem(String configId) {
        return hash.get(configId);
    }

    public void removeItem(String configId) {
        if (hash.containsKey(configId)) {
            hash.remove(configId);
        }
    }
}
