/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.core.dtm;

import com.inspur.edp.bef.api.action.determination.IDeterminationContext;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.operation.Determination;
import com.inspur.edp.bef.engine.ErrorCodes;
import com.inspur.edp.bef.engine.BefCoreExceptionBase;
import com.inspur.edp.bef.engine.common.GspBizEntityUtil;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.determination.AbstractDeterminationAction;
import com.inspur.edp.cef.spi.determination.IDetermination;
import com.inspur.edp.bef.engine.event.query.BefEngineQueryEventBroker;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

public class AfterQueryDtmAdaptor implements IDetermination {

    private CommonDetermination determination;
    private GspBizEntityObject gspBizEntityObject;
    private Object String;

    public AfterQueryDtmAdaptor(GspBizEntityObject gspBizEntityObject,
                                CommonDetermination determination) {
        this.gspBizEntityObject = gspBizEntityObject;
        this.determination = determination;
    }

    @Override
    public String getName() {
        return determination.getName();
    }

    @Override
    public boolean canExecute(IChangeDetail change) {
        return true;
    }

    private Constructor constructor;

    private Constructor getConstrutor() {
        if (constructor == null) {
            constructor = GspBizEntityUtil.getConstructor(
                    determination.getComponentId(), IDeterminationContext.class, IChangeDetail.class);
        }
        return constructor;
    }

    @Override
    public void execute(ICefDeterminationContext context, IChangeDetail change) {
        try {
            AbstractDeterminationAction determinationAction = GspBizEntityUtil.
                    getComponentClass(determination.getComponentId(), Arrays.asList((IDeterminationContext) context, change));
            BefEngineQueryEventBroker.fireProcessRecordClassPath(determinationAction);
            determinationAction.Do();
        } catch (Exception e) {
            throw new BefCoreExceptionBase(e);
        }
    }
}
