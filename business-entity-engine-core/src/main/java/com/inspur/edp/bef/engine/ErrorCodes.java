package com.inspur.edp.bef.engine;

public class ErrorCodes {
    /**
     * 无法实例化的类型{0}
     * classFullName
     */
    public static String BEF_ENGINE_1001 = "BEF_ENGINE_1001";
    /**
     * 找不到字段{0}.{1}
     */
    public static String BEF_ENGINE_1002 = "BEF_ENGINE_1002";
    /**
     * 找不到be自定义动作{0}.{1}
     */
    public static String BEF_ENGINE_1003 = "BEF_ENGINE_1003";
    /**
     * 找不到be实体动作{0}.{1}
     */
    public static String BEF_ENGINE_1004 = "BEF_ENGINE_1004";
    /**
     * 动作对应的构件元数据不存在{0}.{1}
     */
    public static String BEF_ENGINE_1005 = "BEF_ENGINE_1005";
    /**
     * 业务实体{0}上的节点{1}的tcc配置{2}没有设置对应的构件，请正确设置构件
     * 0:业务实体 1:节点 2:tcc配置
     */
    public static String BEF_ENGINE_1006 = "BEF_ENGINE_1006";
    /**
     * 不支持的数据对象类型{0}
     */
    public static String BEF_ENGINE_1007 = "BEF_ENGINE_1007";
    /**
     * 不支持的字段数据类型{0}
     */
    public static String BEF_ENGINE_1008 = "BEF_ENGINE_1008";
    /**
     * 获取ID为{0}的udt元数据失败
     */
    public static String BEF_ENGINE_1009 = "BEF_ENGINE_1009";
    /**
     * 字段{0}默认值{1}不存在,请修改
     */
    public static String BEF_ENGINE_1010 = "BEF_ENGINE_1010";
    /**
     * 多值UDT表达式异常，请联系管理员
     */
    public static String BEF_ENGINE_1011 = "BEF_ENGINE_1011";
    /**
     * 事件{0}上定义的子节点{1}触发条件无效,可以尝试重新设置触发条件
     */
    public static String BEF_ENGINE_1012 = "BEF_ENGINE_1012";
    /**
     * field字段不允许为空
     */
    public static String BEF_ENGINE_1013 = "BEF_ENGINE_1013";
    /**
     * {0}{1}{2}的模型的节点{3}的字段{4}上设置的默认值{5}不存在, 请联系元数据开发人员修改
     * 0:模型ID 1:模型编号 2:模型名称 3:节点 4:字段 5:字段设置默认值
     */
    public static String BEF_ENGINE_1014 = "BEF_ENGINE_1014";
    /**
     * {0}{1}{2}的模型的节点{3}的字段{4}上设置的默认值{5}格式不正确, 请联系元数据开发人员修改
     * 0:模型ID 1:模型编号 2:模型名称 3:节点 4:字段 5:字段设置默认值
     */
    public static String BEF_ENGINE_1015 = "BEF_ENGINE_1015";

}
