/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.core.val;

import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.beenum.BETriggerTimePointType;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.Validation;
import com.inspur.edp.bef.bizentity.operation.collection.ValidationCollection;
import com.inspur.edp.bef.core.validation.ValidationContext;
import com.inspur.edp.bef.engine.common.GspBizEntityUtil;
import com.inspur.edp.cef.api.dataType.base.ICefDataTypeContext;
import com.inspur.edp.cef.api.dataType.entity.ICefEntityContext;
import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.core.validation.builtinimpls.CefB4SaveValAssembler;
import com.inspur.edp.cef.designtime.api.collection.CommonValCollection;
import com.inspur.edp.cef.designtime.api.operation.CommonValidation;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;

public class BeforeSaveVal extends CefB4SaveValAssembler {

    public BeforeSaveVal(GspBusinessEntity gspBusinessEntity, GspBizEntityObject gspBizEntityObject) {
        initValidations(gspBusinessEntity, gspBizEntityObject);
    }

    public void initValidations(GspBusinessEntity gspBusinessEntity, GspBizEntityObject gspBizEntityObject) {
        //todo 内置校验规则后续添加
        CommonValCollection validationCollection = gspBizEntityObject.getallValBeforeSave();
        for (CommonValidation commonValidation : validationCollection) {
            if (commonValidation == null) {
                continue;
            }
            if (ValComponentManager.isBuildInValidation(commonValidation.getComponentId())) {
                BuiltInValidationValAdaptor builtInValidationValAdaptor = new BuiltInValidationValAdaptor(gspBizEntityObject, commonValidation, BETriggerTimePointType.BeforeCheck);
                getValidations().add(builtInValidationValAdaptor);
            } else {
                CommonEngineValAdaptor commonEngineValAdaptor = new CommonEngineValAdaptor(gspBusinessEntity, gspBizEntityObject, commonValidation, BETriggerTimePointType.BeforeCheck);
                getValidations().add(commonEngineValAdaptor);
            }
        }
    }

    @Override
    public ICefValidationContext getValidationContext(ICefEntityContext contePar) {
        return new ValidationContext((IBENodeEntityContext) contePar);
    }

    @Override
    public IChangeDetail getChangeset(ICefDataTypeContext dataTypeContext) {
        return ((IBENodeEntityContext) dataTypeContext).getCurrentTransactionalChange();
    }
}
