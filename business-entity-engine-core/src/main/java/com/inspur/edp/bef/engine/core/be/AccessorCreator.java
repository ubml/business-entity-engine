/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.core.be;

import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.engine.entity.EngineRootAccessor;
import com.inspur.edp.cef.api.dataType.base.IAccessorCreator;
import com.inspur.edp.cef.entity.accessor.base.IAccessor;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefEntityResInfoImpl;
import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefModelResInfoImpl;

public class AccessorCreator implements IAccessorCreator {
    private final CefModelResInfoImpl resInfo;

    private GspBusinessEntity be;

    public AccessorCreator(GspBusinessEntity be, CefModelResInfoImpl resInfo) {
        this.be = be;
        this.resInfo = resInfo;
    }

    @Override
    public IAccessor createAccessor(ICefData data) {
        return new EngineRootAccessor(be.getMainObject(), (IEntityData) data, getResInfo(be.getMainObject().getCode()));
    }

    @Override
    public IAccessor createReadonlyAccessor(ICefData data) {
        return new EngineRootAccessor(be.getMainObject(), (IEntityData) data, getResInfo(be.getMainObject().getCode()), true);
    }

    private CefEntityResInfoImpl getResInfo(String nodeCode) {
        return (CefEntityResInfoImpl) resInfo.getCustomResource(nodeCode);
    }
}
