/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.core.dtm;

import com.inspur.edp.bef.api.action.determination.IQueryDeterminationContext;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.operation.BizCommonDetermination;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameterCollection;
import com.inspur.edp.bef.bizentity.operation.componentinterface.IBizParameter;
import com.inspur.edp.bef.engine.BefCoreExceptionBase;
import com.inspur.edp.bef.engine.common.GspBizEntityUtil;
import com.inspur.edp.bef.spi.action.determination.AbstractQueryDetermination;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.determination.IDetermination;

import java.util.Arrays;

public class BeforeQueryDtmAdaptor implements IDetermination {

    private CommonDetermination determination;
    private GspBizEntityObject gspBizEntityObject;

    private int paramCount = 0;

    public BeforeQueryDtmAdaptor(GspBizEntityObject gspBizEntityObject, CommonDetermination determination) {
        this.gspBizEntityObject = gspBizEntityObject;
        this.determination = determination;
    }

    @Override
    public String getName() {
        return determination.getName();
    }

    @Override
    public boolean canExecute(IChangeDetail change) {
        return true;
    }

//    private Constructor constructor;
//
//    private Constructor getConstrutor() throws ClassNotFoundException {
//        if (constructor == null) {
//            if (paramCount > 0) {
//                GspComponent beComponent = CMEngineUtil.getMetadataContent(determination.getComponentId());
//                Class clazz = Class.forName(beComponent.getMethod().getClassName());
//                constructor = clazz.getConstructors()[0];
//            } else {
//                constructor = GspBizEntityUtil
//                        .getConstructor(determination.getComponentId(), IQueryDeterminationContext.class);
//            }
//        }
//        return constructor;
//    }

    @Override
    public void execute(ICefDeterminationContext context, IChangeDetail change) {
        try {
            paramCount = 0;
            BizCommonDetermination bizCommonDetermination = null;
            if (!(this.determination instanceof BizCommonDetermination)) {
                return;
            }
            bizCommonDetermination = (BizCommonDetermination) this.determination;
            BizParameterCollection collection = bizCommonDetermination.getParameterCollection();
            paramCount = collection == null ? 0 : collection.getCount();
            Object[] args = new Object[paramCount + 1];
            args[0] = (IQueryDeterminationContext) context;
            if (paramCount > 0) {
                IBizParameter parameter =null;
                for (int i = 0; i < paramCount; i++) {
                    parameter = collection.getItem(i);
                    if(parameter!=null && parameter.getActualValue()!=null){
                        args[1 + i] = collection.getItem(i).getActualValue().getValue();
                    }else{
                        args[1 + i] = null;
                    }
                }
            }
            AbstractQueryDetermination determinationAction = GspBizEntityUtil.
                    getComponentClass(this.determination.getComponentId(), Arrays.asList(args));
            determinationAction.Do();
        } catch (Exception e) {
            throw new BefCoreExceptionBase(e);
        }
    }
}
