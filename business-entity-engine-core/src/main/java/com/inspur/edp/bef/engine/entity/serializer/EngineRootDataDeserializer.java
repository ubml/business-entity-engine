/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.entity.serializer;

import com.inspur.edp.bef.engine.common.GspBizEntityUtil;
import com.inspur.edp.bef.engine.entity.exception.BefEngineExceptionBase;
import com.inspur.edp.bef.spi.entity.AbstractBizEntityDeSerializer;
import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefEntityResInfoImpl;
import com.inspur.edp.cef.spi.extend.entity.ICefAddedChildEntityExtend;
import com.inspur.edp.cef.spi.extend.entity.ICefEntityExtend;
import com.inspur.edp.cef.spi.jsonser.entity.AbstractEntitySerializerItem;
import com.inspur.edp.das.commonmodel.IGspCommonModel;
import com.inspur.edp.das.commonmodel.IGspCommonObject;

import java.util.ArrayList;
import java.util.List;

public class EngineRootDataDeserializer extends AbstractBizEntityDeSerializer {

    private final IGspCommonModel commonModel;
    private CefEntityResInfoImpl resInfo;

    public EngineRootDataDeserializer(IGspCommonModel commonModel, CefEntityResInfoImpl resInfo) {
        this(commonModel, resInfo, commonModel.getGeneratedConfigID());
    }

    public EngineRootDataDeserializer(IGspCommonModel commonModel, CefEntityResInfoImpl resInfo, String beType) {
        super(commonModel.getMainObject().getCode(), true, beType, getTypes(resInfo));
        this.commonModel = commonModel;
        this.resInfo = resInfo;
    }

    private static List<AbstractEntitySerializerItem> getTypes(CefEntityResInfoImpl resInfo) {
        java.util.ArrayList<AbstractEntitySerializerItem> list = new ArrayList<>();
        list.add(new com.inspur.edp.cef.spi.jsonser.builtinimpls.CefEntityDataSerializerItem(resInfo));
        return list;
    }

    @Override
    protected AbstractBizEntityDeSerializer getChildConvertor(String childCode) {
        IGspCommonObject childNode = GspBizEntityUtil
                .checkNodeExists(commonModel.getMainObject(), childCode, null);
        return new EngineChildDataDeserializer(childNode,
                (CefEntityResInfoImpl) resInfo.getChildEntityResInfo(childNode.getCode()));
    }

    @Override
    protected String getRealChildCode(String propertyName) {
        propertyName = propertyName.substring(0, propertyName.length() - 1);
        IGspCommonObject childNode = GspBizEntityUtil
                .getChildObject(commonModel.getMainObject(), propertyName);
        if (childNode == null) {
            if (this.getExtendList() != null && this.getExtendList().size() > 0) {
                for (Object extend : this.getExtendList()) {
                    ICefEntityExtend entendType = (ICefEntityExtend) extend;
                    List<ICefAddedChildEntityExtend> extChilds = entendType.getAddedChilds();
                    if (extChilds != null && extChilds.size() > 0) {
                        for (ICefAddedChildEntityExtend child : extChilds) {
                            if (child.getNodeCode().equalsIgnoreCase(propertyName)) {
                                return child.getNodeCode();
                            }
                        }
                    }
                }
            }
            throw new BefEngineExceptionBase("childcode not found:" + propertyName);
        } else {
            return childNode.getCode();
        }
    }
}
