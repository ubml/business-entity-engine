/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.core.dtm;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.beenum.BETriggerTimePointType;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.Determination;
import com.inspur.edp.bef.bizentity.operation.collection.DeterminationCollection;
import com.inspur.edp.bef.core.determination.AbstractB4QueryDtmAssembler;
import com.inspur.edp.bef.core.determination.builtinimpls.BefB4QueryDtmAssembler;
import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.spi.determination.IDetermination;

import java.util.ArrayList;
import java.util.List;

public class BeforeQueryDtm extends BefB4QueryDtmAssembler {


    public BeforeQueryDtm(GspBizEntityObject node) {
        initDeterminations(node);
    }


    private void initDeterminations(GspBizEntityObject node) {
        for (CommonDetermination commonDetermination : node.getDtmBeforeQuery()) {
            BeforeQueryDtmAdaptor afterCreateDtmAdaptor = new BeforeQueryDtmAdaptor(node,
                    commonDetermination);
            getDeterminations().add(afterCreateDtmAdaptor);
        }
    }
}
