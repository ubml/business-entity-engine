/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.common;

import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.component.base.GspComponent;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class BizOperationUtil {
    private static final HashMap<String, String> compMap = new HashMap<>();


    static {
//        compMap.put("aa213406-922e-473b-997b-017a82321b4d", "ee86e96e-6d51-48c7-9e92-9b7dfe3b43b6");//批量新增附件信息
        compMap.put("ea8405f5-ee3f-4870-9fc7-648ca431a210", "2e1beb7d-ad8f-4da3-a430-c8a7f2162135&&a0b19650-0101-468f-ae3f-40c76c0f06b0");//取消审批构件   todo 需要传 参数数组【流程实例id,状态】
        compMap.put("62fd76df-88a8-4576-a2f8-03fad8bbbf4a", "2e1beb7d-ad8f-4da3-a430-c8a7f2162135&&a0b19650-0101-468f-ae3f-40c76c0f06b0");//提交审批构件   todo 需要传 参数数组【流程实例id,状态】
        compMap.put("d0745e53-4872-41e3-be4e-87dc6f34813b", "2e1beb7d-ad8f-4da3-a430-c8a7f2162135&&a0b19650-0101-468f-ae3f-40c76c0f06b0");//更新流程实例与状态   todo 需要传 参数数组【流程实例id,状态】
        compMap.put("8db497fc-e55d-478e-9b9d-aa59966b9599", "2e1beb7d-ad8f-4da3-a430-c8a7f2162135&&a0b19650-0101-468f-ae3f-40c76c0f06b0");//子表更新流程实例构件   todo 需要传 参数数组【流程实例id,状态】
//        compMap.put("b5ce8542-7084-4bf1-ab00-eda1412efc5f", "ee86e96e-6d51-48c7-9e92-9b7dfe3b43b6");//更新附件信息
        compMap.put("9b3888a5-6d88-490b-aa9c-e2790348c6c9", "12be876c-368c-4262-88ab-4112688540b0");//子对象父节点分级方式新增子级
        compMap.put("353f49d2-713f-4439-855a-4eeb463f8647", "12be876c-368c-4262-88ab-4112688540b0");//子对象父节点分级方式新增同级
        compMap.put("86b5d020-4937-497e-942d-f869d572794f", "dbfbe55d-ba65-4a7f-a9d4-4f664ec6ec68");//子对象分级码分级方式新增子级
        compMap.put("d03d585d-8788-40fb-8b0e-dbeb7a96c949", "dbfbe55d-ba65-4a7f-a9d4-4f664ec6ec68");//子对象分级码分级方式新增同级
        compMap.put("b04e0a22-fddb-45dd-a091-4bb46b65bc59", "646e833d-131d-489a-abee-7095723f77ec||41d13ab3-47a6-490c-bb9a-818354df6b93");//停用 todo 待更新
        compMap.put("576811d2-dd42-4b7d-9da5-6831c5eaa856", "646e833d-131d-489a-abee-7095723f77ec||41d13ab3-47a6-490c-bb9a-818354df6b93");//启用 todo 待更新
        compMap.put("bd5cd392-ce3d-4ad6-b3ce-2616a756a237", "12be876c-368c-4262-88ab-4112688540b0");//父节点分级方式新增子级
        compMap.put("dac98710-78c1-42cd-8954-857f7117ff1e", "12be876c-368c-4262-88ab-4112688540b0");//父节点分级方式新增同级
        compMap.put("2f446058-90be-4cfb-8870-580b82dbc991", "dbfbe55d-ba65-4a7f-a9d4-4f664ec6ec68");//分级码分级方式新增子级
        compMap.put("344519d9-b271-4aa0-8800-48a8762fbe20", "dbfbe55d-ba65-4a7f-a9d4-4f664ec6ec68");//分级码分级方式新增子级
    }

    /**
     * 是否内置自定义动作
     *
     * @param componentId
     * @return
     */
    public static boolean isBuildInAction(String componentId) {
        return compMap.containsKey(componentId);
    }

    /**
     * 获取审批 参数数组
     *
     * @param gspBizEntityObject
     * @param componentId
     * @return
     */
    public static String[] getArrayPropName(GspBizEntityObject gspBizEntityObject, String componentId) {
        String propName = "";
        String[] argsArray = new String[2];
        String[] compArray = compMap.get(componentId).split("&&");
        for (int i = 0; i < compArray.length; i++) {
            for (IGspCommonField gspCommonField : gspBizEntityObject.getContainElements()) {
                if (gspCommonField.getIsUdt() && gspCommonField.getUdtID().equals(compArray[i])) {
                    argsArray[i] = gspCommonField.getLabelID();
                }
            }
        }
        return argsArray;
    }

    /**
     * 获取停用启用属性名
     *
     * @param gspBizEntityObject
     * @param componentId
     * @return
     */
    public static String getEnableDisablePropName(GspBizEntityObject gspBizEntityObject, String componentId) {
        String propName = "";
        String[] compArray = compMap.get(componentId).split("\\|\\|");
        for (int i = 0; i < compArray.length; i++) {
            for (IGspCommonField gspCommonField : gspBizEntityObject.getContainElements()) {
                if (gspCommonField.getIsUdt() && gspCommonField.getUdtID().equals(compArray[i])) {
                    return gspCommonField.getLabelID();
                }
            }
        }
        return propName;
    }

    public static String getPropName(GspBizEntityObject gspBizEntityObject, String componentId) {
        String propName = "";
        for (IGspCommonField gspCommonField : gspBizEntityObject.getContainElements()) {
            if (gspCommonField.getIsUdt() && gspCommonField.getUdtID().equals(compMap.get(componentId))) {
                return gspCommonField.getLabelID();
            }
        }
        return propName;
    }

    public static Object getInstance(GspBusinessEntity be, BizOperation op, IBEManagerContext context, GspComponent metadata, Object... pars) {
        String typeName = metadata.getMethod().getClassName();

        Object[] args = new Object[pars != null ? pars.length + 2 : 2];
        args[0] = context;
        GspBizEntityObject obj = getObj(be, pars);
        if (pars != null) {
            System.arraycopy(pars, 0, args, 1, pars.length);
            //pars实际不会是null,是个空数组，为了规避代码检查解引用，挪到这里面来
            if (compMap.get(op.getComponentId()).contains("&&")) {//审批动作，需要传递参数数组
                args[pars.length + 1] = BizOperationUtil.getArrayPropName(obj, op.getComponentId());
            } else if (compMap.get(op.getComponentId()).contains("||")) {//停用启用
                args[pars.length + 1] = BizOperationUtil.getEnableDisablePropName(obj, op.getComponentId());
            } else {
                args[pars.length + 1] = BizOperationUtil.getPropName(obj, op.getComponentId());
            }
        }

//            //约定 构造函数用后面的
////            Constructor[] constructors = Class.forName(typeName).getConstructors();
////            constructor = constructors[0];
////            //获取参数最多的构造函数
////            for (Constructor cons : constructors) {
////                if (constructor.getGenericParameterTypes().length < cons.getGenericParameterTypes().length) {
////                    constructor = cons;
////                }
////            }
////            return constructor.newInstance(args);
        return GspBizEntityUtil.getComponentClass(op.getComponentId(), Arrays.asList(args));
    }

    private static GspBizEntityObject getObj(GspBusinessEntity be, Object[] pars) {
        if (pars != null && pars.length >= 2) {
            List<Object> nodes = (ArrayList) pars[0];
            String targetNode = (String) nodes.get(nodes.size() - 1);
            return (GspBizEntityObject) GspBizEntityUtil.findNode(be, targetNode);
        } else {
            return be.getMainObject();
        }
    }
}
