/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.core.dtm;

import com.inspur.edp.bef.api.action.determination.IDeterminationContext;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.operation.Determination;
import com.inspur.edp.bef.component.base.GspComponent;
import com.inspur.edp.bef.engine.common.GspBizEntityUtil;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.determination.AbstractDeterminationAction;
import com.inspur.edp.cef.spi.determination.IDetermination;
import com.inspur.edp.commonmodel.engine.api.common.CMEngineUtil;

import java.util.Arrays;

public class AfterCreateDtmAdaptor implements IDetermination {
    //    private Determination determination;
    private CommonDetermination commonDetermination;
    private GspBizEntityObject gspBizEntityObject;
    private Object String;

    public AfterCreateDtmAdaptor(GspBizEntityObject gspBizEntityObject, Determination determination) {
        this.gspBizEntityObject = gspBizEntityObject;
//        this.determination = determination;
    }

    public AfterCreateDtmAdaptor(GspBizEntityObject gspBizEntityObject, CommonDetermination determination) {
        this.gspBizEntityObject = gspBizEntityObject;
        this.commonDetermination = determination;
    }

    @Override
    public String getName() {
        return commonDetermination.getName();
    }

    @Override
    public boolean canExecute(IChangeDetail change) {
        return change.getChangeType() == ChangeType.Added;
    }

    @Override
    public void execute(ICefDeterminationContext context, IChangeDetail change) {
        GspComponent beComponent = CMEngineUtil.getMetadataContent(this.commonDetermination.getComponentId());
        AbstractDeterminationAction determinationAction = GspBizEntityUtil.
                getComponentClass(beComponent.getComponentID(), Arrays.asList((IDeterminationContext) context, change));
        determinationAction.Do();
//        catch (NoSuchMethodException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        } catch (InstantiationException e) {
//            e.printStackTrace();
//        } catch (InvocationTargetException e) {
//            e.printStackTrace();
//        }
    }
}
