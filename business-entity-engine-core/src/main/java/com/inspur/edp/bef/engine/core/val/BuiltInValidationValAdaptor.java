/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.core.val;

import com.inspur.edp.bef.api.action.validation.IValidationContext;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.beenum.BETriggerTimePointType;
import com.inspur.edp.bef.bizentity.beenum.RequestNodeTriggerType;
import com.inspur.edp.bef.bizentity.common.BefDtBeanUtil;
import com.inspur.edp.bef.bizentity.operation.Validation;
import com.inspur.edp.bef.component.base.GspComponent;
import com.inspur.edp.bef.engine.common.GspBizEntityUtil;
import com.inspur.edp.bef.spi.action.validation.AbstractValidation;
import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.designtime.api.operation.CommonValidation;
import com.inspur.edp.cef.designtime.api.operation.ExecutingDataStatus;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.validation.IValidation;
import com.inspur.edp.commonmodel.engine.api.common.CMEngineUtil;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.metadata.rtcustomization.api.CustomizationRtService;
import com.inspur.edp.metadata.rtcustomization.api.CustomizationService;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 内置udt校验规则
 */
public class BuiltInValidationValAdaptor implements IValidation {
    private CommonValidation validation;
    private GspBizEntityObject gspBizEntityObject;
    private BETriggerTimePointType beTriggerTimePointType;

    public BuiltInValidationValAdaptor(GspBizEntityObject gspBizEntityObject, CommonValidation validation, BETriggerTimePointType beTriggerTimePointType) {
        this.gspBizEntityObject = gspBizEntityObject;
        this.validation = validation;
        this.beTriggerTimePointType = beTriggerTimePointType;
    }

    @Override
    public boolean canExecute(IChangeDetail change) {
        ExecutingDataStatus temp = ExecutingDataStatus.None;

        switch (change.getChangeType()) {
            case Added:
                temp = ExecutingDataStatus.added;
                break;
            case Modify:
                temp = ExecutingDataStatus.Modify;
                break;
            case Deleted:
                temp = ExecutingDataStatus.Deleted;
                break;
        }
        boolean triggerVaild = false;
        //todo 这个不大对
        for (ExecutingDataStatus triggerTimePointType : this.validation.getGetExecutingDataStatus()) {
            if (temp == triggerTimePointType) {
                triggerVaild = true;
            }
        }
        if (!triggerVaild)
            return false;
        //触发字段？
        if (this.validation.getRequestElements().getCount() > 0) {
            List<String> list = new ArrayList();
            for (String item : this.validation.getRequestElements()) {
                IGspCommonElement gspCommonElement = gspBizEntityObject.getContainElements().getItem(item);
                list.add(gspCommonElement.getLabelID());
            }
            String[] elements = list.toArray(new String[0]);
            if (com.inspur.edp.cef.entity.changeset.Util.containsAny(change, elements)) {
                return true;
            }
        } else {
            return true;
        }
        return false;
    }

    @Override
    public void execute(ICefValidationContext compContext, IChangeDetail change) {
        //获取密级属性字段值
        GspComponent beComponent = CMEngineUtil.getMetadataContent(validation.getComponentId());
//            Class clazz = Class.forName(beComponent.getMethod().getClassName());
        String propertyName = ValComponentManager.getPropName(gspBizEntityObject, this.validation.getComponentId());
//            Constructor constructor = clazz.getConstructor(IValidationContext.class, IChangeDetail.class, String.class);
//            AbstractValidation validationAction=(AbstractValidation)  constructor.newInstance((com.inspur.edp.bef.api.action.validation.IValidationContext)compContext,change, propertyName);

        AbstractValidation validationAction = GspBizEntityUtil.
                getComponentClass(this.validation.getComponentId(), Arrays.asList((IValidationContext) compContext, change, propertyName));

        validationAction.Do();
    }
}
