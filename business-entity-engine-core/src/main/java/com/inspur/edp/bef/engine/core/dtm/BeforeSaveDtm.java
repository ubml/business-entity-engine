/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.core.dtm;

import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.beenum.BETriggerTimePointType;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.Determination;
import com.inspur.edp.bef.bizentity.operation.collection.DeterminationCollection;
import com.inspur.edp.bef.core.determination.DeterminationContext;
import com.inspur.edp.bef.core.determination.builtinimpls.BefB4SaveDtmAssembler;
import com.inspur.edp.cef.api.dataType.base.ICefDataTypeContext;
import com.inspur.edp.cef.api.dataType.entity.ICefEntityContext;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.core.determination.builtinimpls.CefB4SaveDtmAssembler;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.collection.CommonDtmCollection;
import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.determination.IDetermination;
import com.inspur.edp.cef.spi.determination.IEntityRTDtmAssembler;
import com.inspur.edp.das.commonmodel.IGspCommonObject;

import java.util.ArrayList;
import java.util.List;

public class BeforeSaveDtm extends BefB4SaveDtmAssembler {
    public BeforeSaveDtm(GspBizEntityObject gspBizEntityObject) {
        initDeterminations(gspBizEntityObject);
    }

    private void initDeterminations(GspBizEntityObject gspBizEntityObject) {
        CommonDtmCollection determinationCollection = gspBizEntityObject.getAllDtmBeforeSave();
        for (CommonDetermination commonDetermination : determinationCollection) {
            if (commonDetermination == null) {
                continue;
            }
            CommonEngineDtmAdaptor commonDtmAdaptor = new CommonEngineDtmAdaptor(gspBizEntityObject, commonDetermination);
            getDeterminations().add(commonDtmAdaptor);
        }

        for (IGspCommonField element : gspBizEntityObject.getContainElements()) {
            if (element.getIsUdt() && DtmComponentManager.getBeforeSaveComponents().contains(element.getUdtID())) {
                BuiltInEngineDtmAdaptor builtInEngineDtmAdaptor = new BuiltInEngineDtmAdaptor(gspBizEntityObject, element, BETriggerTimePointType.BeforeCheck);
                getDeterminations().add(builtInEngineDtmAdaptor);
            }
        }
    }

    @Override
    public ICefDeterminationContext getDeterminationContext(ICefEntityContext dataTypeContext) {
        return new DeterminationContext((IBENodeEntityContext) dataTypeContext);
    }

    @Override
    public IChangeDetail getChangeset(ICefDataTypeContext dataTypeContext) {
        return ((IBENodeEntityContext) dataTypeContext).getTransactionalChange();
    }
}
