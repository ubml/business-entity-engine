/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.entity.serializer;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.spi.entity.AbstractBizEntityChangeJsonSerializer;
import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefEntityResInfoImpl;
import com.inspur.edp.cef.spi.jsonser.entity.AbstractEntityChangeSerializer;
import com.inspur.edp.cef.spi.jsonser.entity.AbstractEntitySerializerItem;
import com.inspur.edp.commonmodel.engine.core.common.CMUtil;
import com.inspur.edp.das.commonmodel.IGspCommonModel;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.formserver.viewmodel.GspViewModel;

import java.util.ArrayList;

public class EngineChangeSerializer extends AbstractBizEntityChangeJsonSerializer {

    private IGspCommonObject node;
    private CefEntityResInfoImpl resInfo;
    private boolean isRoot = false;

    public EngineChangeSerializer(IGspCommonModel commonModel, CefEntityResInfoImpl resInfo) {
        super(commonModel.getMainObject().getCode(), true, commonModel.getGeneratedConfigID(), getTypes(resInfo));
        this.node = commonModel.getMainObject();
        this.resInfo = resInfo;
        this.isRoot = true;
    }

    public EngineChangeSerializer(GspBizEntityObject beNode, CefEntityResInfoImpl resInfo) {
        super(beNode.getCode(), false, beNode.getBelongModel().getGeneratedConfigID(), getTypes(resInfo));
        this.node = beNode;
        this.resInfo = resInfo;
        this.isRoot = false;
    }

    private static java.util.ArrayList<AbstractEntitySerializerItem> getTypes(CefEntityResInfoImpl entityResInfo) {
        java.util.ArrayList<AbstractEntitySerializerItem> list = new ArrayList<>();
        list.add(new com.inspur.edp.cef.spi.jsonser.builtinimpls.CefEntityDataSerializerItem(entityResInfo));
        return list;
    }

    @Override
    protected AbstractEntityChangeSerializer getChildEntityConvertor(String childCode) {
        IGspCommonObject childNode = CMUtil.checkNodeExists(node, childCode, null);
        return new EngineChangeSerializer((GspBizEntityObject) childNode,
                (CefEntityResInfoImpl) resInfo.getChildEntityResInfo(childNode.getCode()));
    }

    @Override
    protected com.inspur.edp.cef.spi.jsonser.abstractcefchange.AbstractCefDataSerializer getCefDataConvertor() {
        return isRoot ? new EngineRootDataSerializer((GspBusinessEntity) node.getBelongModel(), resInfo) : new EngineChildDataSerializer(node, resInfo);
    }

}

