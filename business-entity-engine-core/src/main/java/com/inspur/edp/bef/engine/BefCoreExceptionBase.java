package com.inspur.edp.bef.engine;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

import java.util.HashMap;

public class BefCoreExceptionBase extends CAFRuntimeException {
    private static final String SU = "pfcommon";
    private static final String RESOURCE_FILE = "bef_engine_core_exception.properties";

    public BefCoreExceptionBase(Exception exception, String errorCodes, String... messageParams) {
        super(SU, RESOURCE_FILE, errorCodes, messageParams, exception, ExceptionLevel.Error, true);
    }

    public BefCoreExceptionBase(String errorcodes, String... messageParams) {
        super(SU, RESOURCE_FILE, errorcodes, messageParams, null, ExceptionLevel.Error, true);
    }

    public BefCoreExceptionBase(Exception e) {
        super(SU, null, null, e);
    }

    public BefCoreExceptionBase(Throwable throwable) {
        super(SU, null, null, null);
    }
}