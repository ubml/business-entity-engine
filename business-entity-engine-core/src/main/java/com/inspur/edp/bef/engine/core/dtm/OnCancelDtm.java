/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.core.dtm;

import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.beenum.BETriggerTimePointType;
import com.inspur.edp.bef.bizentity.operation.Determination;
import com.inspur.edp.bef.core.determination.DeterminationContext;
import com.inspur.edp.bef.core.determination.builtinimpls.BefOnCancelDtmAssembler;
import com.inspur.edp.cef.api.dataType.base.ICefDataTypeContext;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.determination.IDetermination;
import com.inspur.edp.cef.spi.determination.IEntityRTDtmAssembler;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class OnCancelDtm extends BefOnCancelDtmAssembler {


    public OnCancelDtm(GspBizEntityObject node) {
        initDeterminations(node);
    }

    private void initDeterminations(GspBizEntityObject node) {
        for (CommonDetermination commonDetermination : node.getDtmCancel()) {
            getDeterminations().add(new CommonEngineDtmAdaptor(node, commonDetermination));
        }
        for (IGspCommonField element : node.getContainElements()) {
            if (!element.getIsUdt()) {
                continue;
            }
            Function<IGspCommonField, IDetermination> builtinDtm = DtmComponentManager.getOnCancelBuiltinComponents()
                    .get(element.getUdtID());
            if (builtinDtm == null) {
                continue;
            }
            getDeterminations().add(builtinDtm.apply(element));
        }
    }

    @Override
    public IChangeDetail getChangeset(ICefDataTypeContext dataTypeContext) {
        return ((IBENodeEntityContext) dataTypeContext).getCurrentTemplateChange();
    }

    @Override
    public com.inspur.edp.cef.api.determination.ICefDeterminationContext getDeterminationContext(
            com.inspur.edp.cef.api.dataType.entity.ICefEntityContext dataTypeContext) {
        return new DeterminationContext((IBENodeEntityContext) dataTypeContext);
    }
}

