/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.core.dtm.builtin;

import com.inspur.edp.bef.api.action.determination.IDeterminationContext;
import com.inspur.edp.bef.engine.BefCoreExceptionBase;
import com.inspur.edp.bef.engine.ErrorCodes;
import com.inspur.edp.bef.engine.core.dtm.IFieldConsumer;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.determination.IDetermination;
import com.inspur.edp.common.component.actionextension.CancelAttachmentTempDeter;

public class AttachOnCancelDtmAdaptor implements IDetermination, IFieldConsumer {

    private IGspCommonField field;

    @Override
    public void setField(IGspCommonField field) {
        this.field = field;
    }

    public String getName() {
        return "CancelAttachmentTempDeter";
    }

    @Override
    public boolean canExecute(IChangeDetail change) {
        if (change.getChangeType() != ChangeType.Deleted) {
            return true;
        }
        return false;
    }

    public void execute(ICefDeterminationContext context, IChangeDetail change) {
        if (field == null) {
            throw new BefCoreExceptionBase(ErrorCodes.BEF_ENGINE_1013);
        }

        new CancelAttachmentTempDeter((IDeterminationContext) context, change, field.getLabelID())
                .execute();
    }
}
