/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.entity.serializer;

import com.inspur.edp.bef.spi.entity.AbstractBizEntityChangeJsonDeSerializer;
import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefEntityResInfoImpl;
import com.inspur.edp.cef.spi.jsonser.abstractcefchange.AbstractCefDataDeSerializer;
import com.inspur.edp.cef.spi.jsonser.entity.AbstractEntityChangeJsonDeSerializer;
import com.inspur.edp.cef.spi.jsonser.entity.AbstractEntitySerializerItem;
import com.inspur.edp.cef.spi.jsonser.util.SerializerUtil;
import com.inspur.edp.commonmodel.engine.core.common.CMUtil;
import com.inspur.edp.das.commonmodel.IGspCommonModel;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.entity.GspCommonObject;

import java.util.ArrayList;

public class EngineChangeDeserializer extends AbstractBizEntityChangeJsonDeSerializer {

    private GspCommonObject node;
    private CefEntityResInfoImpl resInfo;
    private boolean isRoot = false;

    public EngineChangeDeserializer(GspCommonObject node, CefEntityResInfoImpl resInfo) {
        this(node, resInfo, node.getBelongModel().getGeneratedConfigID());
        isRoot = false;
    }

    public EngineChangeDeserializer(GspCommonObject node, CefEntityResInfoImpl resInfo, String beType) {
        super(node.getCode(), false, beType, getTypes(resInfo));
        this.node = node;
        this.resInfo = resInfo;
        isRoot = false;
    }

    public EngineChangeDeserializer(IGspCommonModel commonModel, CefEntityResInfoImpl resInfo) {
        this(commonModel, resInfo, commonModel.getGeneratedConfigID());
    }

    public EngineChangeDeserializer(IGspCommonModel commonModel, CefEntityResInfoImpl resInfo, String beType) {
        super(commonModel.getMainObject().getCode(), true, beType,
                getTypes(resInfo));
        this.node = (GspCommonObject) commonModel.getMainObject();
        this.resInfo = resInfo;
        isRoot = true;
    }

    private static ArrayList<AbstractEntitySerializerItem> getTypes(CefEntityResInfoImpl resInfo) {
        java.util.ArrayList<AbstractEntitySerializerItem> list = new ArrayList<>();
        list.add(new com.inspur.edp.cef.spi.jsonser.builtinimpls.CefEntityDataSerializerItem(resInfo));
        return list;
    }

    @Override
    protected AbstractEntityChangeJsonDeSerializer getChildEntityConvertor(String childCode) {
        GspCommonObject childNode = (GspCommonObject) CMUtil.checkNodeExists(node, childCode, null);
        return new EngineChangeDeserializer(childNode,
                (CefEntityResInfoImpl) resInfo.getChildEntityResInfo(childNode.getCode()), this.beType);
    }

    @Override
    protected boolean isChildObjectCode(String childCode) {
        IGspCommonObject childObj = node.getContainChildObjects().stream()
                .filter((item) -> SerializerUtil
                        .isChildCodeEquals(childCode, item.getCode() + "s")).findFirst().orElse(null);
        return childObj != null;
    }

    @Override
    protected AbstractCefDataDeSerializer getCefDataDeSerializer() {
        return isRoot ? new EngineRootDataDeserializer(node.getBelongModel(), resInfo, this.beType)
                : new EngineChildDataDeserializer(node, resInfo, this.beType);
    }
}

