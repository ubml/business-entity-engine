/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.core.dtm;

import com.inspur.edp.bef.api.action.determination.IDeterminationContext;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.beenum.BETriggerTimePointType;
import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.validation.IValidation;
import com.inspur.edp.common.component.actionextension.*;


/**
 * 内置udt构件适配
 */
public class BuiltInEngineValAdaptor implements IValidation {
    private IGspCommonField gspCommonField;
    private GspBizEntityObject gspBizEntityObject;
    private Object String;
    private BETriggerTimePointType triggerType;//触发时机

    public BuiltInEngineValAdaptor(GspBizEntityObject gspBizEntityObject, IGspCommonField gspCommonField, BETriggerTimePointType triggerType){
        this.gspBizEntityObject = gspBizEntityObject;
        this.gspCommonField = gspCommonField;
        this.triggerType = triggerType;
    }

    @Override
    public boolean canExecute(IChangeDetail change) {
        if(gspCommonField.getUdtID().equals("ee86e96e-6d51-48c7-9e92-9b7dfe3b43b6")){//附件
            return true;
        }
        return true;
    }

    @Override
    public void execute(ICefValidationContext context, IChangeDetail change) {

        if(gspCommonField.getUdtID().equals("ee86e96e-6d51-48c7-9e92-9b7dfe3b43b6")){//附件
            if(triggerType == BETriggerTimePointType.AfterSave){//保存后
                new AttachmentAfterValDetermination((IDeterminationContext)context, change, gspCommonField.getLabelID()).execute();
            }

        }
    }
}
