/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.core;

import com.inspur.edp.bef.core.be.builtinimpls.BeEntityCacheInfo;
import com.inspur.edp.bef.core.be.builtinimpls.BefManagerCacheInfo;

import java.util.HashMap;
import java.util.Map;

public class BefEngineInfo {

    private BefManagerCacheInfo managerCacheInfo = new BefManagerCacheInfo();

    public BefManagerCacheInfo getManagerCacheInfo() {
        return managerCacheInfo;
    }

    private Map<String, BeEntityCacheInfo> entityCacheInfoMap = new HashMap<>();

    public BeEntityCacheInfo getBeEntityCacheInfo(String entityId) {
        if (entityCacheInfoMap.containsKey(entityId))
            return entityCacheInfoMap.get(entityId);
        entityCacheInfoMap.put(entityId, new BeEntityCacheInfo());
        return entityCacheInfoMap.get(entityId);
    }

    private String cacheVersion = "";

    public String getCacheVersion() {
        return cacheVersion;
    }

    public void setCacheVersion(String cacheVersion) {
        this.cacheVersion = cacheVersion;
    }

}
