/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.core.dtm;

import com.inspur.edp.bef.api.action.IBefCallContext;
import com.inspur.edp.bef.api.action.determination.IDeterminationContext;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.operation.BizCommonDetermination;
import com.inspur.edp.bef.bizentity.operation.beomponent.DeterminationParameter;
import com.inspur.edp.bef.bizentity.operation.componentenum.BizParameterType;
import com.inspur.edp.bef.bizentity.operation.componentinterface.IBizParameter;
import com.inspur.edp.bef.core.determination.builtinimpls.predicate.ExecutePredicate;
import com.inspur.edp.bef.engine.common.GspBizEntityUtil;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.entity.changeset.*;
import com.inspur.edp.cef.spi.determination.AbstractDeterminationAction;
import com.inspur.edp.cef.spi.determination.IDetermination;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.Arrays;

public class CommonEngineDtmAdaptor implements IDetermination {

    private final ExecutePredicate canExecutePredicate;
    private CommonDetermination determination;
    private GspBizEntityObject gspBizEntityObject;
    private int paramCount = 0;

    public CommonEngineDtmAdaptor(GspBizEntityObject gspBizEntityObject,
                                  CommonDetermination determination) {
        this.gspBizEntityObject = gspBizEntityObject;
        this.determination = determination;
        this.canExecutePredicate = DtmUtil.buildCanExecute(gspBizEntityObject, determination);
    }

    @Override
    public String getName() {
        return determination.getName();
    }

    @Override
    public boolean canExecute(IChangeDetail change) {
        return canExecutePredicate.test(change);
    }

    @Override
    public void execute(ICefDeterminationContext context, IChangeDetail change) {
        BizCommonDetermination bizCommonDetermination = (BizCommonDetermination) determination;
        if (runOnceCheck((IDeterminationContext) context, bizCommonDetermination)) {
            return;
        }
        paramCount = 0;
        bizCommonDetermination = (BizCommonDetermination) this.determination;
        paramCount = bizCommonDetermination.getParameterCollection() == null ? 0 : bizCommonDetermination.getParameterCollection().getCount();

        Object[] args = new Object[paramCount + 2];
        args[0] = context;
        args[1] = change;
        if (paramCount > 0) {
            for (int i = 0; i < paramCount; i++) {
                args[2 + i] = getParamValue(bizCommonDetermination.getParameterCollection().getItem(i));
            }
        }
        AbstractDeterminationAction determinationAction = GspBizEntityUtil.
                getComponentClass(this.determination.getComponentId(), Arrays.asList(args));
        determinationAction.Do();
    }

    private Object getParamValue(IBizParameter parameter) {
        if (parameter.getActualValue() == null || StringUtils.isEmpty(parameter.getActualValue().getValue()))
            return null;
        String value = parameter.getActualValue().getValue();
        if (parameter.getParameterType() == BizParameterType.String) {
            return value;
        } else if (parameter.getParameterType() == BizParameterType.Int32) {
            return Integer.valueOf(value);
        } else if (parameter.getParameterType() == BizParameterType.Boolean) {
            return Boolean.valueOf(value);
        } else if (parameter.getParameterType() == BizParameterType.Decimal) {
            return new BigDecimal(value);
        }
        return value;
    }

    private boolean runOnceCheck(IDeterminationContext context,
                                 BizCommonDetermination bizCommonDetermination) {
        if (bizCommonDetermination.getRunOnce()) {
            IBefCallContext callContext = context.getBEContext().getCallContext();
            String mark = determination.getID().concat(context.getBEContext().getID());
            if (callContext.containsKey(mark))
                return true;
            callContext.setData(mark, null);
        }
        return false;
    }

    @Override
    public String toString() {
        return (gspBizEntityObject != null ? gspBizEntityObject.getCode() : "nonNode")
                + ":"
                + (determination != null ? determination.getCode() : "nonOp");
    }
}
