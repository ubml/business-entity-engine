/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.core.lcp;

import com.inspur.edp.bef.api.be.BEInfo;
import com.inspur.edp.bef.api.be.IBEManager;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.common.BefDtBeanUtil;
import com.inspur.edp.bef.core.lcp.StandardLcp;
import com.inspur.edp.bef.engine.ErrorCodes;
import com.inspur.edp.bef.engine.BefCoreExceptionBase;
import com.inspur.edp.bef.engine.api.IEngineStandardLcp;
import com.inspur.edp.bef.engine.common.GspBizEntityUtil;
import com.inspur.edp.bef.engine.core.be.EngineBEManager;
import com.inspur.edp.bef.spi.extend.IBEManagerExtend;
import com.inspur.edp.cef.entity.condition.EntityFilter;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.commonmodel.engine.api.common.CMEngineUtil;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.metadata.rtcustomization.api.CustomizationRtService;
import com.inspur.edp.metadata.rtcustomization.api.CustomizationService;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import org.springframework.util.StringUtils;

public class EngineStandardLcp extends StandardLcp implements IEngineStandardLcp {

    private GspBusinessEntity be;
    private GspMetadata metadata;

    @Deprecated
    public EngineStandardLcp(String sessionId, String metadataId) {
        super(sessionId);
        initMetadata(metadataId);
    }

    public GspBusinessEntity getBE() {
        return be;
    }

    public EngineStandardLcp(String metadataId) {
        super();
        initMetadata(metadataId);
    }

    private void initMetadata(String metadataId) {
        Objects.requireNonNull(metadataId, "metadataId");
        be = CMEngineUtil.getMetadataContent(metadataId);
        metadata = CMEngineUtil.getMetadata(metadataId);
        Objects.requireNonNull(be, "be");
    }

    @Override
    protected HashMap<String, String> getNodeRecord() {
        ArrayList<IGspCommonObject> nodes = be.getAllObjectList();
        HashMap<String, String> map = new HashMap(nodes.size());
        for (IGspCommonObject node : nodes) {
            map.put(node.getCode(), node.getID());
        }
        return map;
    }

    @Override
    protected IBEManager createBEManager() {
        return new EngineBEManager(be, metadata);
    }

    @Override
    public Object executeCustomAction(String actionCode, Object... pars) {
        return getEngineBEManager().executeCustomAction(actionCode, pars);
    }

    private EngineBEManager getEngineBEManager() {
        return (EngineBEManager) getBEManager();
    }

    @Override
    protected BEInfo innerGetBEInfo() {
        BEInfo info = new BEInfo();
        info.setBEType(be.getGeneratedConfigID());
        info.setBEID(be.getID());
        return info;
    }

    @Override
    protected List<IBEManagerExtend> getExtends() {
        String beType = getBEType();
        if (StringUtils.isEmpty(beType)) {
            return null;
        }
        String beId = getBEInfo().getBEID();
        if (StringUtils.isEmpty(beId) || beId.equals(be.getId())) {
            return null;
        }

        String classFullName = "com.inspur.edp.customize.bef.BefCtmManagerExtend";
        try {
            Class providerClass = Class.forName(classFullName);
            Constructor c = providerClass.getConstructor(String.class);
            return java.util.Arrays.asList((IBEManagerExtend) c.newInstance(beType));
        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException |
                 InstantiationException e) {
            throw new BefCoreExceptionBase(e, ErrorCodes.BEF_ENGINE_1001, classFullName);
        }
    }
    //  @Override
//  protected BEInfo innerGetBEInfo() {
//    return new BEInfo() {{
//      setBEType(be.getGeneratedConfigID());
//      setBEID(be.getID());
//      setRootEntityCode(be.getCode());
//    }};
//  }

    //测试用
//  private static int flag = 0;
//
//  @Override
//  public List<IEntityData> query(EntityFilter filter) {
//    switch (flag) {
//      case 0:{
//        String orgId = "011325a0-594b-4063-b391-577d4b1b2fbc";
//        IEntityData orgData = retrieve(orgId).getData();
//        retrieveDefault(orgData);
//        save();
//        break;
//      }
//
//      case 1:{
//        String orgId = "011325a0-594b-4063-b391-577d4b1b2fbc";
//        IEntityData orgData = retrieve(orgId).getData();
//        this.cleanUp();
//        modify(orgData);
//        save();
//        break;
//      }
//
//      case 2:{
//        String actionCode = "testListen";
//        executeCustomAction(actionCode, "testpar1");
//        break;
//      }
//    }
//    return null;
//  }
}
