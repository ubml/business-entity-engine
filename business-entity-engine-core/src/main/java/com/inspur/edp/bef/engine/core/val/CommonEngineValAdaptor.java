/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.core.val;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.beenum.BETriggerTimePointType;
import com.inspur.edp.bef.bizentity.operation.BizCommonValdation;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameterCollection;
import com.inspur.edp.bef.bizentity.operation.componentinterface.IBizParameter;
import com.inspur.edp.bef.core.determination.builtinimpls.predicate.ExecutePredicate;
import com.inspur.edp.bef.engine.common.GspBizEntityUtil;
import com.inspur.edp.bef.spi.action.validation.AbstractValidation;
import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.designtime.api.operation.CommonValidation;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.validation.IValidation;

import java.util.Arrays;

public class CommonEngineValAdaptor implements IValidation {

    private final ExecutePredicate canExecutePredicate;
    private CommonValidation validation;
    private GspBizEntityObject gspBizEntityObject;
    private GspBusinessEntity gspBusinessEntity;
    private BETriggerTimePointType beTriggerTimePointType;


//  public CommonEngineValAdaptor(GspBizEntityObject gspBizEntityObject, CommonValidation validation,
//      BETriggerTimePointType beTriggerTimePointType) {
//    this.gspBizEntityObject = gspBizEntityObject;
//    this.validation = validation;
//    this.beTriggerTimePointType = beTriggerTimePointType;
//  }

    public CommonEngineValAdaptor(GspBusinessEntity gspBusinessEntity,
                                  GspBizEntityObject gspBizEntityObject, CommonValidation validation,
                                  BETriggerTimePointType beTriggerTimePointType) {
        this.gspBusinessEntity = gspBusinessEntity;
        this.gspBizEntityObject = gspBizEntityObject;
        this.validation = validation;
        this.beTriggerTimePointType = beTriggerTimePointType;
        this.canExecutePredicate = ValUtil.buildCanExecute(gspBizEntityObject, validation);

    }

    @Override
    public boolean canExecute(IChangeDetail change) {
        return canExecutePredicate.test(change);
    }

    @Override
    public void execute(ICefValidationContext compContext, IChangeDetail change) {
        if (this.validation == null || !(this.validation instanceof BizCommonValdation)) {
            return;
        }

        BizCommonValdation bizCommonValdation = (BizCommonValdation) this.validation;
        BizParameterCollection collection = bizCommonValdation.getParameterCollection();
        int paramCount = collection == null ? 0 : collection.getCount();

        Object[] args = new Object[paramCount + 2];
        args[0] = compContext;
        args[1] = change;
        if (paramCount > 0) {
            IBizParameter parameter = null;
            for (int i = 0; i < paramCount; i++) {
                parameter = collection.getItem(i);
                if(parameter!=null && parameter.getActualValue()!=null){
                    args[2 + i] = parameter.getActualValue().getValue();
                }else{
                    args[2 + i] = null;
                }
            }
        }
        //      AbstractValidation validationAction = (AbstractValidation) getConstructor()
//          .newInstance(args);
        AbstractValidation validationAction = GspBizEntityUtil.
                getComponentClass(this.validation.getComponentId(), Arrays.asList(args));
        validationAction.Do();

    }

    @Override
    public String toString() {
        return (gspBizEntityObject != null ? gspBizEntityObject.getCode() : "nonNode")
                + ":"
                + (validation != null ? validation.getCode() : "nonOp");
    }

    //  private volatile Constructor ctor;
//
//  private Constructor getConstructor() throws ClassNotFoundException, NoSuchMethodException {
//    if (ctor == null) {
//      GspComponent beComponent = CMEngineUtil.getMetadataContent(validation.getComponentId());
//      Class clazz = Class.forName(beComponent.getMethod().getClassName());
//      if(paramCount > 0){
//        Constructor constructor = clazz
//                .getConstructors()[0];
//        ctor = constructor;
//      }
//      else{
//        Constructor constructor = clazz
//                .getConstructor(IValidationContext.class, IChangeDetail.class);
//        ctor = constructor;
//      }
//    }
//    return ctor;
//  }
}
