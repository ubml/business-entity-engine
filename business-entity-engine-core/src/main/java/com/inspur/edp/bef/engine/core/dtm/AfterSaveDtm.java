/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.engine.core.dtm;

import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.beenum.BETriggerTimePointType;
import com.inspur.edp.bef.core.determination.DeterminationContext;
import com.inspur.edp.bef.core.determination.builtinimpls.BefAfterSaveDtmAssembler;
import com.inspur.edp.cef.api.dataType.base.ICefDataTypeContext;
import com.inspur.edp.cef.api.dataType.entity.ICefEntityContext;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.collection.CommonDtmCollection;
import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;

public class AfterSaveDtm extends BefAfterSaveDtmAssembler {
    public AfterSaveDtm(GspBizEntityObject gspBizEntityObject) {
        initDeterminations(gspBizEntityObject);
    }

    private void initDeterminations(GspBizEntityObject gspBizEntityObject) {
        CommonDtmCollection determinationCollection = gspBizEntityObject.getDtmAfterSave();
        for (CommonDetermination commonDetermination : determinationCollection) {
            if (commonDetermination == null) {
                continue;
            }
            CommonEngineDtmAdaptor commonDtmAdaptor = new CommonEngineDtmAdaptor(gspBizEntityObject, commonDetermination);
            getDeterminations().add(commonDtmAdaptor);
        }
    }

    @Override
    public ICefDeterminationContext getDeterminationContext(ICefEntityContext dataTypeContext) {
        return new DeterminationContext((IBENodeEntityContext) dataTypeContext);
    }

    @Override
    public IChangeDetail getChangeset(ICefDataTypeContext dataTypeContext) {
        return ((IBENodeEntityContext) dataTypeContext).getTransactionalChange();
    }
}
