package com.inspur.edp.commonmodel.engine.api.exception;

public class ErrorCodes {
    /**
     * 关联带出字段不存在{0}.{1}
     */
    public static String CM_ENGINE_0001 = "CM_ENGINE_0001";
    /**
     * 关联(association)为空
     */
    public static String CM_ENGINE_0002 = "CM_ENGINE_0002";
    /**
     * 元数据{0}不存在
     * 0:元数据ID
     */
    public static String CM_ENGINE_0003 = "CM_ENGINE_0003";
}
