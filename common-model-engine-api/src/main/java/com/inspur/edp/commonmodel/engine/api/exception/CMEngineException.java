package com.inspur.edp.commonmodel.engine.api.exception;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

/**
 * @className: CMEngineException
 * @author: wangmj
 * @date: 2023/9/16
 **/
public class CMEngineException extends CAFRuntimeException {
    private static final String SU = "pfcommon";

    private static final String RESOURCE_FILE = "cm_engine_api_exception.properties";

    public CMEngineException(Exception exception, String errorCodes, String... messageParams) {
        super(SU, RESOURCE_FILE, errorCodes, messageParams, exception, ExceptionLevel.Error, true);
    }

    public CMEngineException(String errorcodes, String... messageParams) {
        super(SU, RESOURCE_FILE, errorcodes, messageParams, null, ExceptionLevel.Error, true);
    }
}
